<h1>Report Exploratory Data Analysis for Feature Selection in Machine Learning</h1>

**Domain: datpt7**
# Mục lục
1. [Mục đích](#1)
2. [Chi tiết](#2)
    1. [Descriptive analysis](#2_1)
    2. [Correlation analysis](#2_2)
    3. [Contextual analysis](#2_3)

# 1. Mục đích <a name="1"></a>
* Có cái nhìn tổng quan về tập data.
* Chất lượng và khả năng dự đoán của các feature.
* Từ đó ta sẽ có các giải pháp cải thiện các feature để quá trình ML đạt kết quả tốt hơn.

# 2. Chi tiết <a name="2"></a>
## 2.1. Descriptive analysis <a name="2_1"></a>
* Phân tích các thông tin của từng attribute, bao gồm:
    * Loại dữ liệu.
    * Dữ liệu bị miss.
    * Đối với biến số: Mean, median, mode, range,...
    * Đối với biến phân loại: Số lượng của mỗi loại,...
* Cân nhắc khi lựa chọn các feature có:
    * Tỉ lệ dữ liệu bị miss quá cao. 
        * Một số loại miss dữ liệu:
            * Missing at random: dữ liệu bị miss không liên quan gì đến miền giá trị của nó mà liên quan đến miền giá trị của các attribute khác. -> Có thể xóa.
            * Missing at complete random: dữ liệu bị miss hoàn toàn ngẫu nhiên. -> Có thể xóa.
            * Missing not at random: dữ liệu bị miss phụ thuộc vào miền giá trị của nó. -> Cân nhắc khi xóa.
    * Low variance of numeric attributes: Là các attribute loại biến số có phương sai nhỏ chứng tỏ dữ liệu bị tập trung vào gần một giá trị dẫn đến không có giá trị không mô hình. -> Có thể xóa.
    * Low entropy of categorical attributes: Là các attribute loại biến phân loại bị tập trung vào 1 loại chứng tỏ đa số dữ liệu đều có 1 loại dẫn đến không có giá trị trong mô hình. -> Có thể xóa.
    * Imbalance of categorical target (class imbalance): Là mất cân bằng trong target, cũng có nghĩa là phần lớn dữ liệu ở target bị tập trung về một loại (major class) trong khi các loại khác (minor class) có rất ít dữ liệu.
        * Có 3 cách xử lý:
            * Undersampling majority class: Loại bỏ ngẫu nhiên các dữ liệu ở major class. Tuy nhiên cách này có vấn đề là dễ bị mất các dữ liệu quan trọng.
            * Oversampling minority class: Nhân bản các dữ liệu ở minor class. Tuy nhiên cách này có vấn đề là không có thông tin mới được thêm vào.
            * Alternative metric and/or loss function: Phạt nặng khi dự đoán sai các dữ liệu ở minor class. Giúp chú ý hơn đến các dữ liệu ở minor class. 
    * Skew distribution: Các attribute dạng số phân bố trải dài (xiêng quá mức).
        * Có 4 cách xử lý:
            * Thay đổi các dữ liệu bị outlier (bắt nguồn từ dữ liệu sai) bằng các giá trị phù hợp nhờ vào kiến thức của vấn đề đó.  
            * Đối với các dữ liệu nằm ở 2 biên nhưng không phải là outlier thì sử dụng log transformations để dữ liệu cân bằng hơn.(Chưa tìm hiểu log transformations)
            * Sử dụng bucketization hoặc binning transformation để tạo ra các nhóm dữ liệu và cân bằng số lượng dữ liệu trong mỗi nhóm đó giúp dữ liệu được cân bằng hơn.
            * Đối với các dữ liệu nằm ở 2 biên là các outlier thì nên loại bỏ một số dữ liệu nằm ở 2 biên để dữ liệu được cân bằng hơn(Lưu ý phải loại bỏ ở cả tập training và tập serving).
    * High cardinality: Attribute có quá nhiều loại nhưng loại có quá ít dữ liệu ở mỗi loại.
        * Một số cách xử lý:
            * Áp dụng hashtrick để chuyển số loại lại thành một lượng nhỏ các loại với kích thước bằng nhau.
            * Xóa tất cả các dữ liệu thuộc các loại có quá ít dữ liệu.

## 2.2 Correlation analysis <a name="2_2"></a>
* Phân tích sự tương quan giữa 2 feature hoặc 1 feature và 1 target.
* 2 khía cạnh quan tâm:
    * Qualitative analysis: biểu thị mối quan hệ bằng biểu đồ.
    * Quantitative analysis: biểu thị mối quan hệ bằng con số cụ thể.
* Cân nhắc khi lựa chọn các feature:
    * Có rất ít sự tương quan giữa feature và target.
        * Nguyên nhân và cách xử lý:
            * Nếu feature này thật sự không liên quan đến target thì xóa.
            * Do cách thể hiện hiện tại của feature khó có thể tìm thấy được sự tương quan đến target nên cần chuyển sang một dạng thể hiện khác (dùng bucketizing).
    * Có quá nhiều sự tương quan giữa các feature: Tốn resource cho cả 2 feature nhưng không có thêm thông tin từ 1 trong 2 feature 
        * Cách xử lý:
            * Loại bỏ tất cả, chỉ để lại 1.
            * Áp dụng nonlinear model ( ví dụ: neural network) để khai thác sự tương quan thay vì dùng linear model. 

## 2.3. Contextual analysis <a name="2_3"></a>
* Phân tích ngữ cảnh để có cái nhìn rõ ràng hơn về tập dữ liệu.
* 2 khía cạnh quan tâm:
    * Time-based analysis
    * Agent-based analysis


