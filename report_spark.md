# SPARK

## 1. Khái niệm Map-Reduce, Partition, Shuffle, Bucketing
### 1.1. Mô hình lập trình MapReduce
* Mô hình lập trình MapReduce được thiết kế độc quyền bởi google.
* MapReduce giúp xử lý các tập dữ liệu lớn song song và phân tán trên 1 cụm máy tính.
* MapReduce bao gồm 4 quá giai đoạn:
    * Splits: Input của quy trình MapReduce sẽ được chia thành các phần có kích thước cố định được gọi là input split. Mỗi input split sẽ được làm input cho giai đoạn tiếp theo, chính là Mapping.
    * Mapping: Nhận input split là đầu vào, xử lý và cho ra output là các tập dữ liệu các dạng key-value.
    * Shuffling: Nhóm các key-value có cùng key lại thành 1 nhóm. Mỗi nhóm được tạo ra sẽ làm input cho giai đoạn tiếp theo, đó là Reducing. 
    * Reducing: Nhận input từ quá trình split, xử lý và cho ra kết quả như bài toán yêu cầu.

### 1.2. Partition <a name="partition"></a>
* Một [RDD](#RDD) bao gồm nhiều partition nhỏ, mỗi partition đại diện cho 1 phần dữ liệu phân tán.
* Mặc định, các partition sẽ được lưu trên RAM.
* Mỗi Executor có thể chứa dữ liệu của 1 hoặc 1 vài partition của 1 RDD.
* Partition kết hợp với cơ chế lazy evaluation giúp xử lý dữ liệu 1 cách nhanh chóng.

### 1.3. Shuffle
* Shuffle là quá trình trộn các partition lại với nhau khi gặp các wide transformation.
* Shuffle sẽ ghi data từ các partition vào đĩa sau đó chuyển thông qua mạng.

### 1.4. Bucketing
* Bucketing là một cách tổ chức dữ liệu trong Spark.
* Bucketing giúp các truy vấn có thể trở nên hiệu quả hơn.
* Bucketing giúp trách việc shuffle khi thực hiện các câu lệnh wide transformation.

## 2. Các định dạng file nén trong hệ thống Hadoop

### 2.1. File .gz
* Là file nén được nén bằng gzip.
* Chưa thông tin hữu ích như tên file gốc và timestamp.
* Cú pháp: `gzip/gunzip [option(s)] file_name(s)`. Ví dụ: `gzip data` Chi tiết về các [option](https://ss64.com/bash/gzip.html).

### 2.2. File .bz2
* Là file nén được nén bằng bzip.
* Tập trung vào việc tránh mất dữ liệu trong quá trình nén nên thời gian nén lâu hơn gzip.
* Cú pháp: `bzip2 [option(s)] file_name(s)`. Ví dụ: `bzip2 data data1`. Chi tiết về các [option](https://ss64.com/bash/bzip2.html).

### 2.3. File .snappy
* Là file nén được tạo bởi snapply.
* Tập trung và tốc độ nén, nên kích thức của tệp sẽ lớn hơn gzip hoặc bzip2.
* Snapply được tích hợp trong Hadoop Common. Chi tiết về các [common](http://hadoop.apache.org/docs/current3/hadoop-project-dist/hadoop-common/build/source/hadoop-common-project/hadoop-common/target/api/org/apache/hadoop/io/compress/SnappyCodec.html).

### 2.4. File .LZO
* Là file nén được tạo bởi lzop.
* Tốc độ nén rất nhanh.
* Cú pháp: `lzop [option(s)] file_name(s)`. Có 4 option:
    * `-1`: Nén với tốc độ nhanh.
    * `-d`: Giải nén.
    * `-9`: Nén với việc đảm bảo chất lượng file.
    * `-x`: Trích xuất.

## 3. Kiến thức cơ bản về định dạng parquet
* Parquest lưu trữ dữ liệu bởi các cột liền kề nhau thay vì lưu các hàng liền kề nhau.
* Dữ liệu được phân vùng theo cả chiều ngang và dọc.
* Parquet khắc phục được các nhược điểm của text, giảm thời gian read/write, nén tốt hơn do dữ liệu dược tổ chức theo cột.


## 4. Khái niệm RDD, DataFrame trong Spark
### 4.1. RDD (Resilient Distributed Datasets) <a name="RDD"></a>
#### 4.1.1. Khái niệm
* RDD là một cấu trúc dữ liệu cơ bản của Spark.
* Là một tập hợp Immutability phân tán.
* Được chia thành các [partition](#partition) lưu trữ trên các worker để xử lý song song.
* Được lưu trên Ram, do đó tăng tốc độ xử lý lên 10-100 lần so với Hadoop MapReduce.
* Sparrk cung cấp các [transformation](#transformation) và [action](#action) để thao tác với RDD.
#### 4.1.2. Transformation <a name="transformation"></a>
* Transformation xử lý lazily, tức là giúp dựng các execution plans.
* Một số transformaition thường dùng:
    * `distinct`: loại bỏ trùng lặp trong RDD.
    * `filter`: dùng để lọc dữ liệu, có thể sử dụng lambda kết hợp với filter.
    * `map`: dùng để chuyển đổi một RDD thành một RDD bằng cách xử lý mỗi phần tử của RDD.
    * `flatMap`: cũng dùng để chuyển đổi một RDD thành một RDD khác như map, nhưng mỗi phần tử của RDD sẽ được chuyển hoặc không chuyển thành nhiều phần tử mới. 
    * `sortBy`: mô tả một hàm để trích xuất dữ liệu từ các object của RDD vầ thực hiện sort.
    * `randomSplit`: nhận một mảng các trọng số va ftạo một random seed, tách các RDD thành các mảng RDD có số lưuọng chia theo trọng số.
#### 4.1.3. Action <a name="action"></a>
* Các transformation chỉ thực hiện khi 1 action được gọi sau đó.
* Một số action thường dùng:
    * `reduce`: thực hiện hàm reduce trên RDD để thu về 1 giá trị duy nhất.
    * `count`: đếm số dòng trong RDD.
    * `countApprox`: đếm xấp xỉ số dòng trong RDD, nhưng phải cung cấp timeout vì có thể không nhận được kết quả.
    * `first`: lấy giá trị đầu tiên của RDD.
    * `take`: lấy một lượng giá trị từ RDD.

### 4.2. DataFrame
* Dataframe là một bộ dữ liệu phân tán được tổ chức lưu trữ bên trong các cột được đặt tên.
* Là một tập hợp Immutability phân tán.
* Dataframe có thể được xây dựng từ nhiều nguồn như: các tập dữ liệu có cấu trúc, bảng trong Hive, RDD,...

## 5. Khái niệm UDF
* UDF (User Defined Functions) là hàm được người dùng định nghĩa để xử lý trên mỗi hàng của dữ liệu.
* Một số hàm để định nghĩa UDF:
    * asNonNullable(): cập nhật UDF thành loại non-nullable.
    * asNondeterministic():  cập nhật UDF thành loại nondeterministic.
    * withName(name: String): cập nhật tên UDF.

## 6. Một số khái niệm quan trọng
### 6.1. Client-mode, cluster-mode
#### 6.1.1. Client-mode
* Khi một spark job được deploy với client-mode thì Spark Driver sẽ được chạy bên trong máy submit job, và đương nhiên là Spark Context sẽ sống ở đây.
* Ta có thể xem log ngay trên máy submit job, nếu nhấn Ctrl + C thì job sẽ bị dừng.
* Thường được sử dụng để debug hoặc cho job cần có sự tương tác.
* Khi một máy submit là máy bên trong cụm thì nên ưu tiên dùng chế độ này, vì nếu dùng cluster thì có thể sẽ tốn thời gian chuyển file jar nếu file jar quá lớn.

#### 6.1.2. Cluster-mode
* Khi một Spark Job được deploy với cluster-mode thì Spark Driver sẽ được chạy trong bất cứ worker nào được chỉ định, và Spark Context sẽ sống ở worker đó. 
* Ta có thể xem log ở giao diện Web.
* Khi một máy submit ở xa với cụm thì nên ưu tiên sử dụng chế độ này, vì nếu dùng client thì hiệu suất của job sẽ bị ảnh hưởng bởi đường truyền mạng.

### 6.2. Driver, worker
#### 6.2.1. Driver
* Khái niệm:
    * Driver là một Java process.
    * Process thực thi được các chương trình viết bằng Scala, Java, Python.
    * Thực thi code và tạo một SparkSession hoặc SparkContext.
    * SparkSession có trách nhiệm tạo các DataFrame, DataSet, RDD, truy vấn SQL, thực hiện Transformation và Action,...
* Chịu trách nhiệm:
    * Hàm main của chương trình được chạy trong một Driver process: tạo SparkSession hoặc SparkContext.
    * Chuyển code của chương trình thành các Task (Transformation và Action) và xác định số Task.
    * Giúp tạo Lineage, Logical Plan và Physical Plan.
        > **_Lineage:_** Khi một RDD mới được tạo từ một RDD cha thông qua Transformatoin thì RDD mới đó sẽ chứa một con trỏ, trỏ đến RDD cha. Spark sẽ theo dõi mối quan hệ giữa các RDD này bằng cách sử dụng một thành phần được gọi là Linage. Khi một RDD bị mất dữ liệu thì Linage sẽ có nhiệm vụ khôi phục lại dữ liệu.
        
        > **_Logical Plan:_** Là kế hoạch mô tả những đầu ra mong đợi sau khi áp dụng một loạt các Transformation như join, filter, where, groupBy,...

        > **_Physical Plan:_** Là kế hoạch quyết định loại join và trình tự thực hiện các lệnh filter, where, groupBy, ...
    * Khi một Physical Plan được tạo, Driver sẽ kết hợp với Cluster Manager lập lịch thực thi những Task.
    * Theo dõi metadata ở các Executor.

#### 6.2.2. Worker
* Worker là thành phần của một cụm Spark cụ thể, nơi mà các executor sống để thực thi các task. Người ta thường gọi là các node tính toán trong Spark.
* Worker chia ram và core được yêu cầu cho các executor thực thi bên trong đó.
* Nếu một worker gặp vấn đề thì những task được của các executor bên trong worker đó sẽ được chuyển sang các executor ở các worker khác.

### 6.3. Executor, memory, cores của worker
* Executor nằm trong các worker, mỗi worker có thể chứa nhiều executor nếu nó có đủ memory và core. 
* Mỗi executor sẽ dùng lượng memory và core cho phép.
* Số lượng core trong mỗi executor nên đặt trong khoảng từ 2 đến 5. Vì nếu quá nhiều core dẫn đến việc có quá nhiều request đọc dữ liệu từ HDFS, làm giảm hiệu suất của executor. Hoặc nếu chỉ có 1 executor thì không tận dụng được ưu điểm xử lý đa luồng của JVM.
* Executor thực thi các task được giao và trả kết quả về cho driver thông qua cluster manager.

### 6.4. Executor, memory, cores của driver
* Driver cũng sử dụng lượng memory và core được yêu cầu để đảm nhiệm các công việc như đã nói ở phần trên.
* Driver đa số không cần xử lý đa luồng nên số lượng core nên đặt mặc định là 1.
* Tuy nhiên để tăng tốc độ xử lý của driver thì lượng memory cần được đặt bằng hoặc lớn hơn lượng memory trong executor.

### 6.5. Applications, tasks, stages, jobs
* Application là một chương trình tính toán dùng để chạy code do người dùng cung cấp. Nó bao gồm driver và các executor.
* Job là một công việc tính toán song song bao gồm nhiều Task. Một job được sinh ra khi có 1 action được gọi trong code.
* Mỗi job sẽ được chia thành nhiều Stage. Một Stage được sinh ra nếu gặp wide transformation (ví dụ: reduceByKey,...)
* Mỗi stage gồm một nhóm các Task, mỗi task trên mỗi partition.





