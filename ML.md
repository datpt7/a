# Nhóm: 
# Mục lục
1. [Thành viên](#1)
2. [Giới thiệu sơ lược về project](#2)
3. [Giải pháp](#3)
4. [Thực nghiệm](#4)
5. [ Phác thảo kế hoạch làm việc](#5)
# 1. Thành viên <a name="1"></a>

<table>
  <thead>  
   <tr> 
    <th>Tên</th>
    <th>MSSV</th>
   </tr>  
  </thead>  
  <tbody>
   <tr>
    <td>Phan Thành Đạt</td>
    <td>18133006</td>
   </tr>
   <tr>
    <td>Lê Chí Hiếu</td>
    <td>18133012</td>
   </tr>
   <tr>
    <td>Đỗ Đình Phùng</td>
    <td>18133040</td>
   </tr>
   <tr>
    <td>Nguyễn Anh Triều</td>
    <td>18133058</td>
   </tr>
  </tbody>
</table>

# 2. Giới thiệu sơ lược về project <a name="2"></a>

* Tên project : ***Phân tích và dự đoán giá vé của các chuyến bay ở Mỹ***.

* Tập dữ liệu: Data_Train.xlsx 

* Link tập dữ liệu: [Dữ liệu](https://www.kaggle.com/nikhilmittal/flight-fare-prediction-mh)

* Mô tả ngắn gọn về project mà nhóm muốn giải quyết: sử dụng Pyspark và thư viện Machine learning( Pyspark.MLlib) với các thuật toán để phân tích và dự đoán giá vé máy bay 

* Mô tả ngắn gọn về tập dữ liệu: 
  * Tập dữ liệu về giá vé của các hãng hàng không *từ tháng 3 đến tháng 6 năm 2019*. 
  * Tập dữ liệu chứa tổng số *10683 dòng và 11 biến*:
    * 6 biến phân loại:
      * `Airline`: Tên của các hãng hàng không.
      * `Source`: Điểm bắt đầu của chuyến bay.
      * `Destination`: Điểm đến của chuyến bay.
      * `Route`:  Lộ trình của chuyến bay.
      * `Total_Stops`: Tổng số điểm dừng giữa đỉ điểm bắt đầu và địa điểm kết thúc.
      * `Additional_Info`: Thông tin bổ sung về chuyến bay.
    * 2 biến số:
      * `Duration`: Tổng thời gian của chuyến bay.
      * `Price`: Giá của vé chuyến bay.
    * 3 biến thời gian:
      * `Date_of_Journey`: Ngày của chuyến bay.
      * `Dep_Time`:  Thời điểm mà chuyến bay bắt đầu đi.
      * `Arrival_Time`:  Thời gian đến của chuyến bay.
       
# 3. Giải pháp <a name="3"></a>
* Nhóm dự định sử dụng hồi quy tuyến tính để dự đoán tệp dữ liệu và dùng phương pháp thử cross validation.

# 4. Thực nghiệm <a name="4"></a>

* Tiền xử lý : Kiểm tra các cột có giá trị NA chuyển đổi các dòng NA thành 'None', tách dữ liệu, thêm cột , xóa cột, ... , chuyển đổi kiểu dữ liệu.

* Thuật toán: Những thuật toán Regression và đánh giá mô hình chọn thuật toán tốt nhất cho bài toán.

* Chạy các thuật toán về *Regression*

* Nhóm dự định thực hiện trên các thuật toán khác?

# 5. Phác thảo kế hoạch làm việc (plan) <a name="5"></a>

* Hiểu về tập dữ liệu và vấn đề cần giải quyết.

* Import dữ liệu và tiền xử lý dữ liệu.

* Xử lý dữ liệu phân loại.

* Tìm và xử lý dữ liệu outliers(nếu có).

* Training dữ liệu trên các mô hình regression.

* Chọn mô hình có kết quả tốt nhất.

* Tinh chỉnh siêu tham số của mô hình.
