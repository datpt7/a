# BACKEND REPORT


## MỤC LỤC
1. [Kiến thức](#kien_thuc)
    1. [Đã học được](#kien_thuc_da_hoc_duoc)
        1. [Git](#git)
        2. [Java](#java)
        3. [Database](#database)
        4. [Thrift](#thrift)
    2. [Còn vướng mắt](#kien_thuc_con_vuong_mat)

2. [Thực hành](#thuc_hanh)
    1. [Đã hoàn thành](#thuc_hanh_da_hoan_thanh)
    2. [Chưa hoàn thành](#thuc_hanh_chua_hoan_thanh)
    3. [Mô tả cách cài đặt và sử dụng service Middleware trên server hoặc một máy tính khác](#cai_dat_middleware)
        1. [Cài đặt](#cai_dat)
        2. [Sử dụng](#su_dung)

## NỘI DUNG
## 1. Kiến thức <a name="kien_thuc"></a>
### 1.1. Đã học được <a name="kien_thuc_da_hoc_duoc"></a>
#### 1.1.1. Git <a name="git"></a>
* ***Remote Repository*** là repository được cài đặt trên server chuyên dụng. Ví dụ: *GitHub, GitLab, Bitbucket,...*

* ***Local Repository*** là repository nằm trên chính máy tính của chúng ta, repository này sẽ đồng bộ hóa với ***Remote repository*** bằng các lệnh của git.

* ***Branch*** là những phân nhánh ghi lại luồng thay đổi của lịch sử, các hoạt động trên mỗi branch sẽ không ảnh hưởng lên các branch khác nên có thể tiến hành nhiều thay đổi đồng thời trên một ***Repository***, giúp giải quyết được nhiều nhiệm vụ cùng lúc.

* ***History*** là lịch sử tất cả các commit trong ***Branch*** hiện tại, trong đó bao gồm những thông tin: *mã hash của commit, mô tả, người tạo commit và ngày tạo commit*.

* ***Git flow*** là một ***Branching model tool*** cung cấp bộ các ***Barnch*** đi kèm với các quy ước và ý nghĩa riêng tương ứng với mỗi ***Branch***. Bộ các ***Branch*** được dùng phổ biến nhất là:
    * ***Master*** được coi là ***Branch*** chính với HEAD phản ánh trạng thái prodution-ready.
    * ***Develope*** được coi là Branch chính với HEAD phản ảnh trạng thái thay đổi mới nhất trong quá trình phát triển, chuẩn bị cho Release tiếp theo.
    * ***Features*** là Branch dùng để thực hiện các chức năng mới cho sản phẩm.
    * ***Release*** là Branch được dùng để làm các công việc để chuẩn bị đưa lên Master Branch, ngoài ra còn để fix một vài bug nhỏ còn tồn đọng và chuẩn bị meta-data (version number, build dates...).
    * ***Hotfixes*** là Branch dùng để fix các bug nghiêm trọng xảy ra sau khi sản phẩm đã được Release.

#### 1.1.2. Java <a name="java"></a>
* ***Lập trình hướng đối tượng*** là phương pháp lập trình lấy đối tượng làm nền tảng để xây dựng chương trình. 
    * Các khái niệm cơ bản đi kèm với lập trình hướng đối tượng bao gồm:
        * ***Đối tượng*** dùng để chỉ cho một thực thể (ví dụ: người, vật,...)
        * ***Lớp*** là một khuôn mẫu, đối tượng là một thể hiện cụ thể dựa trên khuôn mẫu đó (ví dụ: đối tượng *Chó Nhật* là thể hiện cụ thể của lớp *Chó*).
    * Lập trình hướng đối tượng có 4 tính chất:
        * ***Tính đóng gói***: các thuộc tính và phương thức có liên quan với nhau được đóng gói thành một lớp để tiện cho việc quản lý và sử dụng.
        * ***Tính đa hình***: các đối tượng thuộc các lớp khác nhau có thể hiểu cùng một thông điệp theo các cách tùy chỉnh khác nhau.
        * ***Tính trừu tượng***: khi thiết kế các đối tượng ta cần rút tỉa ra những đặc trưng chung của chúng rồi trừu tượng hóa thành các *interface* và thiết kế xem chúng sẽ tương tác với nhau như thế nào.
        * ***Tính kế thừa***: một lớp có thể sử dụng lại thuộc tính và phương thức của lớp khác giúp cho chương trình ngắn gọn hơn


* ***Lập trình hàm*** là phương pháp lập trình dựa trên các hàm toán học (function), tránh việc thay đổi giá trị của dữ liệu. Lập trình hàm có 2 tính chất:
    * ***Purity***: tất cả các function đều không có hiệu ứng phụ (side effect), không được tác động lên bất cứ giá trị nào bên ngoài nó, đồng thời cũng không sửa giá trị các đối số truyền vào.
    * ***Immutability***: các đối tượng và các biến không bao giờ bị thay đổi giá trị.

* ***Java virtual machine*** là máy ảo Java, được dùng để chạy các chương trình của Java, hay nói cách khác là trình thông dịch của Java.

* ***Thành phần trong bộ nhớ trong JVM***
    * ***Heap memory***: dùng để lưu các đối tượng và dữ liệu động.
    * ***Thread stacks***: dùng để lưu dữ liệu tĩnh bao gồm các thông về các hàm, phương thức và cả con trỏ đến các đối tượng ở Heap.
    * ***Meta space***: dùng để lưu thông tin về các lớp.
    * ***Code cache***: đây là nơi JIT (Just In Time) lưu trữ những code (đã được biên dịch) thường xuyên được sử dụng.
    * ***Shared libraries***: bao gồm những file thư viện được sử dụng bởi nhiều ứng dụng, giúp giảm số lựong thư viện trùng lặp trong hệ thống.
* ***Garbage Collection: trình dọn rác trong JVM***
    * Thuật toán thường được sử dụng trong Garbage collection là Mark & Sweep bao gồm 3 quá trình: 
        * Marking: Đánh dấu vùng nhớ nào đang được sử dụng và không được sử dụng.
        * Sweeping: Làm sách các vùng nhớ không còn được sử dụng.
        * Compacting: Chuyển các vùng nhớ đang được sử dụng lại với nhau để  vùng trống
    * GC có 2 thành phần: Minor GC và Major GC
        * Minor GC
            * Hoạt động ở Young generation space.
            * Được kích hoạt khi Eden space bị đầy không thể cấp phát vùng nhớ để lưu đối tượng mới.
            * Khi Minor GC được kích hoạt nhiều lần, đương nhiên các vùng nhớ không còn được sử dụng sẽ bị làm sạch, các vùng nhớ còn được sử dụng sẽ được chuyển qua lại giữa 2 Survivor space nếu chưa đủ tuổi, còn khi đủ tuổi sẽ được chuyển thẳng vào Young generation space.
        * Major GC
            * Hoạt động ở Old generation space.
            * Được kích hoạt khi:
                * Một trong các lệnh sau đây được gọi: `System.gc()`, `Runtime.getRuntime().gc()`.
                * Old generation space bị đầy không thể cấp phát vùng nhớ để chuyển từ Young generation space.
                * Bộ nhớ tối đa được cấp cho Meta space quá nhỏ không đủ để lưu các lớp mới.
* ***Gradle***: là một build automation tool và open-source. Các task cơ bản của Gradle:
    * Chạy project: `gradle run`.
    * Chạy test cho project: `gradle test`, kết quả của test sẽ lưu dưới dạng file html ở thư mục: `thu_muc_goc/ten_module/build/reports/tests/`.
    * Build project: `gradle build`, 2 file sẽ được tạo với định dạng tar và zip được lưu ở thư mục: `ten_module/build/distributions/`.
    * Clean folder build: `gralde builld`.
    * Tạo file jar: `gradle jar`.

* ***Các loại tests trong software development***
    * ***Functional testing*** là kiểm tra xem hệ thống có hoạt động đúng theo các yêu cầu nghiệp vụ hay không.
    * ***Non functional testing*** là đặc tính chất lượng của hệ thống sẽ được kiểm tra.
    * ***Structural testing*** là kiểm thử dựa trên phân tích cấu trúc bên trong của thành phần hoặc hệ thống.
    * ***Regression Testing*** là kiểm thử xem lỗi sau khi được sửa đã hoạt động mà không làm ảnh hưởng đến các chức năng khác hay không.

#### 1.1.3. Database <a name="database"></a>   
* ***Các loại database***

| Loại database     | Khái niệm     | Ưu điểm       | Nhược điểm        |
| :--- | :--- | :--- | :--- |
| RDBMS | Tổ chức dữ liệu theo các bảng và có quan hệ với nhau để giảm thiểu sự dư thừa dữ liệu đồng thời vẫn đảm bảo sự hiệu quả trong lưu trữ và truy xuất dữ liệu | + Phù hợp với hầu hết các loại dữ liệu thường dùng nhờ vào cấu trúc đơn giản <br> + Cập nhật dữ liệu nhanh: khi một record được cập nhật, tất cả các record liên quan đến nó cũng sẽ được cập nhật theo <br> + Hỗ trợ atomic transaction giúp tránh sai xót của dữ liệu <br>| + Dữ liệu ở các bảng càng lớn thì thời gian truy xuất càng lâu<br> + Mở rộng database bằng cách thêm bộ nhớ cho máy tính (Vertical Scaling), mà bộ nhớ thì có giới hạn <br>|
| Document database | Tổ chức dữ liệu một cách tự do không theo một lược đồ cố định nào cả | + Cho phép lưu trữ với nhiều cấu trúc khác nhau <br> + Thời gian truy xuất nhanh, không phụ thuộc vào kích thức của database, hỗ trợ các tác vụ song song <br> + Mở rộng database bằng cách thêm máy tính vào hệ lưu trữ phân tán (Horizontal Scaling), không giới hạn bộ nhớ <br> | + Cập nhật dữ liệu chậm vì dữ liệu được nhân bản và lưu ở nhiều máy khác nhau <br> + Không hỗ trợ atomic transaction |
| Columnar database | Tổ chức dữ liệu theo dạng cột thay vì dạng hàng như RDBMS | + Truy xuất nhanh hơn dạng hàng vì chỉ tập trung truy xuất những giá trị cần thiết <br> + Lưu ở dạng cột giúp cho việc phân tích dễ dàng hơn <br> | + Đối với một số truy vấn cần nhiều cột thì lưu trữ theo dạng hàng là sự lựa chọn hợp lý hơn <br> + Thêm dữ liệu mới tốt thời gian hơn dạng hàng thì cần phải thêm từng cột riêng lẻ <br> |
| Key-value database | Tổ chức dữ liệu theo dạng key-value, có nghĩa là với mỗi key sẽ được gắn với một giá trị | + Truy vấn rất nhanh bởi các key là unique và phần lớn key-value đưuọc lưu trữ trên RAM <br> + Cả key và value có thể là bất cứ loại dữ nào từ đơn giản đến phức tạp <br> | Tốn chi phí hơn bởi hoạt động trên RAM | 
| Graph database | Tổ chức dữ liệu để coi các mối quan hệ giữa các dữ liệu là quan trọng như nhau đối với bản thân dữ liệu | + Tốc độ truy vấn chỉ phụ thuộc vào số lượng mối quan hệ cụ thể chứ không phụ thuộc vào lượng dữ liêu <br> + Kết quả được trả về real time <br> + Những mối quan hệ được thể hiện rõ ràng và dễ quản lý <br> + Cấu trúc linh hoạt và nhanh nhẹn <br> | + Khó mở rộng quy mô <br> + Không có ngôn ngữ truy vấn thống nhất <br> |

* ***Docker***
    * Docker là nền tảng cho phép dựng, kiểm thử và triển khai ứng dụng một cách nhanh chóng.
    * Docker đóng gói phần mềm vào các đơn vị tiêu chuẩn hóa được gọi là container.
    * Docker giúp triển khai và thay đổi quy mô ứng dụng vào bất kỳ môi trường nào và biết chắc rằng ứng dụng sẽ chạy được.

* ***Container***
    * Container là đơn vị phần mềm cung cấp cơ chế đóng gói ứng dụng, mã nguồn, thiết lập, thư viện… vào một đối tượng duy nhất. 
    * Ứng dụng sau khi được đóng gói có thể hoạt động một cách nhanh chóng và hiệu quả trên các môi trường khác nhau.  

#### 1.1.1. Thrift <a name="thrift"></a>
* ***Microservices*** là một kiểu kiến trúc phần mềm. Các module trong phần mềm sẽ được chia thành các service, sao cho mỗi service đảm nhận một chức năng. 
    * Ưu điểm:
        * Chính vì chia làm nhiều service như vậy, Microservices giúp dễ nâng cấp và mở rộng hệ thống.
        * Khả năng sập cả hệ thống khi một module bị lỗi rất thấp.
        * Ở mỗi service ta có thể viết các ngôn ngữ các nhau, điều này rất hữu ích vì mỗi ngôn ngữ có một thế mạnh riêng.
    * Nhược điểm:
        * Bởi giao tiếp qua mạng nên tốc độ có tụt lại so với Monolith.
        * Cần phải có phương án cho việc đồng nhất dữ liệu.
        * Việc quản lý nhiều service có phần rắc rối.
        * Cần một đội ngũ đủ mạnh để thiết kế và triển khai hệ thống.

* ***Apache thrift***
    * Khái niệm:
        * Là một ***RPC*** thuộc loại stack software độc lập với ngôn ngữ lập trình với một cơ chế sinh code tự động.
        * Cho phép các xây dựng ***RPC Client*** và ***Server*** bằng cách chỉ việc định nghĩa kiểu dữ liệu và service interface trong một file định nghĩa đơn giản bằng ***IDL***.
        * Hỗ trợ hơn 20 ngôn ngữ lập trình.
    * ***Physical*** là tầng dưới cùng của Thrift, tầng Protocal sẽ ghi vào đây.
    * ***Transport*** là tầng nằm trên và sẽ ghi vào Physical, chịu trách nhiệm về việc dữ liệu được truyền đi như thế nào.
    * ***Protocol*** là tầng nằm trên Transport, chịu trách nhiệm về việc dữ liệu gì được truyền đi bằng cách cung cấp các `serialize` và `deserialize`.
    * ***Processor*** là tầng đọc dữ liệu từ đầu vào, sau đó xử lý dữ liệu và ghi dữ liệu vào đầu ra.
    * ***Kiểu dữ liệu***
        * Cơ bản: `bool, byte, i16, i32, i64, double, string`.
        * Đặc biệt: `binary`.
        * Structs: là một tập các trường có kiểu dữ liệu cơ bản, tương tự như struct trong *C*.
        * Containers: `list, set, map`.
        * Exception: kế thừa các base class Exception tương ứng với từng ngôn ngữ lập trình.
        * Service: bao gồm một tập các hàm, mỗi hàm có một tập các tham số và một kiểu trả về.
* ***Phân biệt Thrift và RESTFull API***
    * Giống: Cả Thrift và RESTFull API đều hỗ trợ cho việc giao tiếp giữa các service trong microservices.
    * Khác:
        
|       | Thrift     | RESTFull API |
| :---          | :---        | :--         |
|   Phổ biến    | Sử dụng nhiều trong các dự án liên quan đến BigData | Sử dụng nhiều trong các ứng dụng Web |
|   Hiệu suất   | Hiệu suất cao | Hiệu suất thấp hơn so với Thrift|
|   Kiểu định dạng   | Hỗ trợ cả text (Json) và binary | Chỉ hỗ trợ text (Json, XML, ...) |
|   Giao thức | Hỗ trợ cả HTTP và TCP/IP | Chỉ hỗ trợ HTTP |
|   Lợi thế    | Đáp ứng được nhiều use case  | Định dạng dữ liệu linh hoạt|


### 1.2. Còn vướng mắt, chưa hiểu <a name="kien_thuc_con_vuong_mat"></a>
* ***Git flow*** tiện dụng như vậy, tại sao thực tế các Developer chỉ áp dụng *Branching Model* theo cách thủ công, có nghĩa là họ merge và tạo các branch một cách thủ công.
* Bản thân chưa có kinh nghiệm về ***RESTFull API***, nên phần so sánh giữa ***Thrift*** và ***RESTFull API*** hơi bị thiên về thế mạnh của ***Thrift***.
## 2. Thực hành <a name="thuc_hanh"></a> 
### 2.1. Đã hoàn thành <a name="thuc_hanh_da_hoan_thanh"></a>
* Tạo 1 ***practice_tests Repository*** trên gitlab để up code.
* Tạo một file ***.thrift*** gồm các service: `nextPrimeNumber, putToDatabase, getFromDatabase`.
* General file ***.thrift*** thành các ***Java class***.
* Tạo 1 ***Server project***: Dùng design pattern Singleton tạo một class Model để impliment lại các service ở class Java đã tạo ở trên.
* Tạo 1 flie ***Dockerfile*** để tích hợp ***Server project*** vào một Container.
* Tạo 1 file ***docker-compose.yml*** để quản lý *Redis container* và *Server container* thay vì phải quản lý riêng lẻ. 
* Tạo 1 ***Client project*** để UnitTest các service.
* Tạo file ***lib.jar*** từ ***Client project*** để import và sử dụng ở các project khác.
* Tạo 1 ***Sample project***, import file jar ở để test các chức năng.

### 2.2. Những vướng mắt hiện tại và hướng giải quyết <a name="thuc_hanh_chua_hoan_thanh"></a>
* Vướng mắt: Khi có nhiều client gọi thực thi method GetFromDatabase thì hệ thống sẽ bị lỗi.
* Hướng giải quyết: server sẽ là instance của class TThreadedSelectorServer thay vì class TSimpleServer.

### 2.3. Mô tả cách cài đặt và sử dụng service Middleware trên server hoặc một máy tính khác <a name="cai_dat_middleware"></a> 
#### 2.3.1. Cài đặt <a name="cai_dat"></a> 
* Download file lib.jar  [ở đây](https://gitlab.com/datpt7/practice_tests/-/blob/main/Sample/app/libs/lib.jar).
* Download docker theo [ubuntu](https://docs.docker.com/engine/install/ubuntu/), [windows](https://docs.docker.com/docker-for-windows/install/) (nếu máy đã có docker thì bỏ qua bước này).
* Tạo một mạng bridge tên server-network với subnet là 172.20.0.0: `docker network create --driver bridge server-network --subnet=172.20.0.0/16`.
* Tạo một folder với tên middleware-docker, thực hiện các bước sau đây ở folder này:
    * Download file docker-compose.yml [ở đây](https://gitlab.com/datpt7/practice_tests/-/blob/main/docker/docker-compose.yml)
    * Tạo một folder server, sau đó download file [sau đây](https://gitlab.com/datpt7/practice_tests/-/blob/main/docker/server/Dockerfile) ở folder đó.
    * Build các image bằng lệnh: `docker-compose build`.
#### 2.3.1. Sử dụng <a name="su_dung"></a> 
* Chạy Redis và Server bằng cách chạy câu lệnh sau: `docker-compose up -d` ở folder middleware-docker.
* Import file lib.jar vào project mà bạn cần sử dụng
* Import thêm các thư viện [javaee-api](https://mvnrepository.com/artifact/javax/javaee-api), [libthrift](https://mvnrepository.com/artifact/org.apache.thrift/libthrift), [slf4j-api](https://mvnrepository.com/artifact/org.slf4j/slf4j-api) và [slf4j-nop](https://mvnrepository.com/artifact/org.slf4j/slf4j-nop).
* Khi sử dụng ở class nào thì `import Middleware.*;` ở class đó.
* Khai báo client, gắn thêm ip của server và port. Ví dụ: `Client client = new Client("172.20.0.3", 9090);`
* Khai báo person dùng để truyền tham số vào khi put. Ví dụ: `Person person = new Person();`
* Sử dụng các phương thức bằng cách gọi tên các phương thức đó. Ví dụ: `client.putToDatabase(person)`, `client.getFromDatabase(int id)`, `client.putMultipleToDatabase(List<person> people)`, `client.getMultipleFromDatabase(List<id> ids)`.



