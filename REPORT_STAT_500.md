# ***[STAT 500](https://online.stat.psu.edu/stat500/)***
**Domain: datpt7**
# Mục lục
1. [Các khái niệm](#1)
2. [Thu thập và tổng hợp dữ liệu](#2)
    1. [Thu thập dữ liệu](#2_1)
    2. [Phân loại dữ liệu](#2_2)
    3. [Tóm tắt và vẽ một biến định tính](#2_3)
    4. [Tóm tắt và vẽ một biến định lượng](#2_4)
3. [Xác suất](#3)
    1. [Tính chất của xác suất](#3_1)
    2. [Xác suất có điều kiện](#3_2)
    3. [Biến cố độc lập](#3_3)
    4. [Định lý Bayes](#3_4)
4. [Phân phối xác suất](#4)
    1. [Phân phối xác suất của biến rời rạc](#4_1)
    2. [Phân phối xác suất của biến liên tục](#4_2)
5. [Phân phối mẫu](#5)
    1. [Phân phối mẫu của trung bình mẫu](#5_1)
    2. [Phân phối mẫu của tỉ lệ mẫu](#5_2)
6. [Ước lượng và khoảng tin cậy](#6)
    1. [Ước lượng cho tỉ lệ trên population](#6_1)
    2. [Ước lượng cho trung bình trên population](#6_2)
7. [Kiểm định giả thuyết](#7)
    1. [Kiểm định tỉ lệ trên 1 mẫu](#7_1)
    2. [Kiểm định trung bình trên 1 mẫu](#7_2)
8. [So sánh 2 parameter của population](#8)
    1. [So sánh 2 tỉ lệ của population](#8_1)
    2. [So sánh 2 trung bình của population](#8_2)
    3. [So sánh 2 độ lệch chuẩn của population](#8_3)
9. [Kiểm định Chi-Square cho những biến độc lập](#9)
10. [Hồi quy tuyến tính](#10)
    1. [Mô hình hồi quy tuyến tính đơn giản](#10_1)
    2. [Hệ số xác định](#10_2)
    3. [Suy luận tương quan](#10_3)
    4. [Mô hình hồi quy tuyến tính đa biến](#10_4)
11. [Giới thiệu về ANOVA](#11)
    1. [Kiểm định giả thuyết một chiều](#11_1)
    2. [Kiểm định nhiều so sánh](#11_2)
12. [Giới thiệu về kiểm định phi tham số và Bootstrap](#12)
    1. [Kiểm định Sign](#12_1)
    2. [Kiểm định Wilcoxon một mẫu](#12_2)
    3. [Phương pháp lấy mẫu Bootstrapping](#12_3)
13. [Tóm tắt các kỹ thuật thống kê](#13)
# 1. Các khái niệm <a name="1"></a>
* Population: là tập hợp toàn bộ tập dữ liệu mà hình quan tâm.
* Parameters: là những con số mô tả population. Ví dụ như trung bình: $$\mu$$, tỉ lệ: $$p$$
* Sample: là tập hợp con của population.
* Statistics: là những con số mô tả sample. Ví dụ như trung bình: $$\bar{x}$$, tỉ lệ: $$\hat{p}$$.
# 2. Thu thập và tổng hợp dữ liệu <a name="2"></a>
## 2.1 Thu thập dữ liệu <a name="2_1"></a>
### 2.1.1 Các loại bias <a name="2_1_1"></a>
* Dữ liệu bị bias với 1 số loại như sau:
    * Non-Response Bias: một lượng lớn những người trong sample không phản hồi hoặc không tham gia.
    * Response Bias: Những người trong sample trả lời không trung thực hoặc trả lời theo câu hỏi.
    * Selection: Sample không tương ứng với population.
### 2.1.2. Các phương pháp thu thập dữ liệu <a name="2_1_2"></a>
* Có 2 phương pháp thu thập dữ liệu:
    * Phương pháp Non-probability:
        * Haphazard: Thu thập dữ liệu từ bất kì ai.
        * Gathering volunteers: Thu thập dữ liệu từ những người tự nguyện cung cấp dữ liệu.
    * Phương pháp Probability:
        * Simple random sample: Các đối tượng trong population đều có cơ hội được chọn như nhau.
        * Stratified random sample: Chia population thành nhiều nhóm, sau đó lấy mẫu ngẫu nhiên bên trong nhóm đó.
        * Cluster sample: Chia population thành nhiều nhóm, sau đó lấy mẫu ngẫu nhiên các nhóm đó.
    * Ở Non-probability thì ta sẽ ít tốn chi phí để thu thập dữ liệu hơn. Tuy nhiên sample được lấy theo cách đó thì thường không đại diện được cho population. Còn ở Probability thì sample sẽ có khả năng đại diện được cho population từ đó các dự đoán hoặc quyết định cho population dựa trên sample sẽ có độ tin cậy cao hơn.
### 2.1.3. Các loại nghiên cứu <a name="2_1_3"></a>
* Observational: Các nhà nghiên cứu quan sát các measure mà không có tác động đến bất kì variable nào. Loại này chỉ nói lên được sự tương quan, không nói lên được kết quả.
* Expermental: Các nhà nghiên cứu tác động lên một số biến và quan sát một số measure. Loại này có thể nói lên mối quan hệ nhân quả.
### 2.1.4. Biến <a name="2_1_4"></a>
* Variable có thể là định tính, định lượng hoặc bất kỳ đại lượng nào được có thể được đo đếm hoặc quan sát để ghi lại.
* Variable trong nghiên cứu chủ yếu được chia thành 2 loại chính và 2 loại phụ:
    * Response variable: Biến mà đang được đặt câu hỏi trên nó.
    * Expalnatory: Biến dùng để giải thích cho biến Response.
* Ngoài ra variable còn được phân vào một số loại nữa như:
    * Lurking variable: Biến mà không phải là biến giải thích cũng không phải là biến trả về, nhưng nó có mối quan hệ với các biến đó. Nó không được xem xét trong nghiên cứu, tuy nhiên nó có thể ảnh hưởng đến mối quan hệ giữa các biến trong nghiên cứu.
    * Confounding variable: Nó là một biến trong nghiên cứu và có liên quan đến các biến khác, do đó nó có ảnh hưởng đến mối quan hệ giữa các biến khác.
### 2.1.5. Nguyên tắc khi thiết kế một nghiên cứu Expermental <a name="2_1_5"></a>
* Control: Cần kiểm soát các tác động do các yếu tố khác với các yếu tố đang quan tâm.
* Randomization: Các đối tượng nên được chia một cách ngẫu nhiên vào các nhóm.
* Replication: Cần sử dụng đủ số lượng đối tượng để đảm bảo rằng việc random ở trên tạo ra sự tương đồng ở các nhóm.
* Lợi ích của Randomization:
    * Random assignment: đảm bảo rằng mỗi đối tượng trong sample đều có cơ hội như nhau để được chỉ định vào bất kỳ nhóm nghiên cứu nào. Từ đó ta có thể đưa ra các kết luận nhân quả.
    * Random selection: đảm bảo rằng mỗi đối tượng trong population đều có cơ hội bình đẳng được chọn làm sample. Từ đó ta có thể sử dụng kết quả trên sample để nói về population.
    * Một nghiên cứu Expermental thì bắt buộc phải sử dụng phương pháp Random assignment. Nếu có thêm phương pháp Random selection nữa thì nghiên cứu đó được gọi là completely randomized experiment.

## 2.2. Phân loại dữ liệu <a name="2_2"></a>
* Dữ liệu được phân thành 2 loại chính:
    * Qualitative: Định tính
        * Binary: Nếu có 2 sự lựa chọn.
        * Ordinal: Nếu các sự lựa chọn có cấp bậc.
        * Nominal: Nếu các sự lựa chọn không có cấp bậc.
    * Quantitative: Định lượng
        * Discrete: Nếu các giá trị số rời rạc.
        * Continuous: Nếu các giá trị số liên tục.

## 2.3. Tóm tắt và vẽ một biến định tính <a name="2_3"></a>
* Cách tốt nhất để tóm tắt một biến định tính là dùng 1 bảng bao gồm tần số và tỉ lệ phần trăm mỗi loại.
* Có 2 loại biểu đồ thể hiện tốt nhất về một biến định tính:
    * Pie chart: Biểu đồ tròn chỉ phù hợp khi biến này có ít loại, nếu có nhiều loại thì ta sẽ gộp các loại có phần trăm thấp lại thành một loại other hoặc ta sẽ chuyển sang Bar chart.
    * Bar chart: Biểu đồ nếu có quá quá nhiều loại, ta cũng sẽ cân nhắc gộp các loại có phần trăm thấp lại thành mọt loại other.

## 2.4. Tóm tắt và vẽ một biến định lượng <a name="2_4"></a>
* Có 3 đại lượng cần phải được thể hiện đó là mean, median và mode.
* Đại lượng tiếp theo là các min, max, Q1, Q3, range, phương sai, độ lệch chuẩn và CV.
* Xuất hiện Outlier có thể ảnh hưởng đến các đại lượng trên, đại lượng bị ảnh hưởng gọi là sensitive, ngược lại đại lượng nào không bị ảnh hưởng thì gọi là resistant.
* Có 4 loại biểu đồ thể hiện tốt nhất về một biến định lượng:
    * Dotplot: Dùng cho các tập dữ liệu nhỏ.
    * Stem-and-leaf Diagram: Dùng cho các tập dữ liệu nhỏ.
    * Histogram: Dùng cho các tập dữ liệu lớn.
    * Boxplot: Dùng cho các tập dữ liệu lớn.

# 3. Xác suất <a name="3"></a>
## 3.1. Tính chất của xác suất <a name="3_1"></a>
* Xác suất của một biến cố A, ký hiệu là $$P(A)$$
* Xác suất luôn nằm trong đoạn $$[0,1]$$
* Tổng xác suất của một biến cố và biến cố đối của nó bằng 1: $$P(A) + P(A') = 1$$
* Nếu A và B là 2 biến cố xung khắc thì $$P(A \cap B) = 0$$ , $$P(A \cup B) = P(A) + P(B)$$
* $$P(A \cup B) = P(A) + P(B) - P(A \cap B)$$
* $$ P(A) = P(A \cap B') + P(A \cap B)$$
## 3.2. Xác suất có điều kiện <a name="3_2"></a>
* Xác suất xảy ra của một biến cố A khi biết một biến cố B đã xảy ra gọi là xác suất có điều kiện.
* Ký hiệu: $$P(A \mid B) = \frac{P(A \cap B)}{P(B)}$$
* $$P(A|B) = 1 - P(A'|B)$$
* $$P(A) = P(A \cap B) + P(A \cap B') = P(A|B)*P(B) + P(A|B')*P(B')$$

## 3.3. Biến cố độc lập <a name="3_3"></a>
* Hai biến cố A, B được gọi là độc lập thì khả năng xảy ra của biến cố A không bị ảnh hưởng cho dù biết hay không biết biến cố B xảy ra hay chưa.
* Nếu một trong những điều kiện sau đây đúng, 2 biến cố A và B được gọi là độc lập:
    * $$P(A \cap B) = P(A)*P(B)$$
    * $$P(A|B)=P(A)$$
    * $$P(B|A)=P(B)$$
## 3.4. Định lý Bayes <a name="3_4"></a>
 ![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-10-07_09-49-25.png) 

# 4. Phân phối xác suất <a name="4"></a>
* Biến ngẫu nhiên là một biến có giá trị không xác định hoặc một hàm số gán giá trị cho từng kết quả của một thí nghiệm.
* Biến ngẫu nhiên có 2 loại:
    * Biến ngẫu nhiên rời rạc: là một loại biến trong đó tất cả các giá trị có thể có của biến ngẫu nhiên nhận một giá trị có thể đếm được.
    * Biến ngẫu nhiên liên tục: là một loại biến trong đó tất cả các giá trị có thể có của biến ngẫu nhiên nhận một giá trị không thể đếm được.
* Hàm xác suất là một hàm tóan học cung cấp xác suất cho các kế t quả có thể xảy ra của biến ngẫu nhiên, ký hiệu f(x). Có 2 loại hàm xác suất:
    * Hàm khối xác suất:
        * Được sử dụng để tính cho biến ngẫu nhiên rời rạc. f(x) = P(X=x) là xác suất để biến ngẫu nhiên X nhận giá trị x
        * Tính chất:
            * f(x) > 0 với x nằm trong không gian mẫu và f(x) = 0 nếu ngược lại
            * $$\sum_{x} f(x) = 1$$. Tổng xác suất của các kết quả có thể có bằng 1.
    * Hàm mật độ xác suất:
        * Được dùng để tính cho biến ngẫu nhiên liên tục. $$f(x) =  P(a < x < b)$$ là diện tích bên dưới f(x) trong khoảng từ a đến b.
        * Tính chất:
            * f(x) > 0 với x nằm trong không gian mẫu và f(x) = 0 nếu ngược lại
            * Tổng toàn miền = 1.
    * Hàm phân phối tích lũy:
        * F(x) = P(X\lex) là xác suất để biến ngẫu nhiên X nhận giá trị bé hơn hoặc bằng x
        * Được dùng để tính cho cả biến ngẫu nhiên liên tục và rời rạc, tuy nhiên ở biến ngẫu nhiên liên tục thì $$P(X\lex) = P(X<x)$$ vì $$P(X=x) = 0$$.

## 4.1. Phân phối xác suất của biến rời rạc <a name="4_1"></a>
* Trung bình của một biến ngẫu nhiên rời rạc: $$\mu = E(X) = \sum_{} x_i f(x_i)$$
* Phương sai của một biến ngẫu nhiên rời rạc: $$\sigma^{2} = Var(X) = \sum_{} (x_i - \mu)^{2}f(x_i) = \sum_{} x_i^{2}f(x_i) - \mu^{2}$$
* Độ lệch chuẩn của một biến ngẫu nhiên rời rạc: $$\sigma = \sqrt{Var(X)}$$
* Phân phối nhị thức là một trường hợp đặc biệt của phân phối rời rạc với chỉ có 2 kết quả đầu ra là success và failse.
    * Với biến ngẫu nhiên X là số lần success của n lần thử thì X là một biến ngẫu nhiên nhị thức.
    * Trung bình của một biến ngẫu nhiên nhị thức: $$\mu = E(X) = np$$. Với p là xác suất success
    * Phương sai của một biến ngẫu nhiên nhị thức: $$Var(X) = np(1-p)$$
    * Độ lệch chuẩn của một biến ngẫu nhiên nhị thức: $$SD(X) =\sqrt{np(1-p)}$$
    * $$f(x) = P(X=x) = \frac{n!}{x!(n-x)!} p^{x} (1-p)^{n-x}$$ với x = 0,1,2,...,n
## 4.2. Phân phối xác suất của biến liên tục <a name="4_2"></a>
* Trung bình của một biến ngẫu nhiên liên tục: $$\mu = E(X) = \int_{-\infty}^{\infty} x f(x) dx$$ 
* Phương sai của một biến ngẫu nhiên liên tục: $$\sigma^{2} = Var(X) = \int_{-\infty}^{\infty} x^2 f(x) d(x) - (\int_{-\infty}^{\infty} x f(x) d(x))^{2}$$
* Độ lệch chuẩn của một biến ngẫu nhiên liên tục: $$\sigma = \sqrt{Var(X)}$$
* Phân phối chuẩn là một trường hợp đặc biệt của phân phối liên tục.
    * Phân phối chuẩn có mean = 0 và phương sai = 1 được gọi là phân phối chuẩn tắc ký hiệu là $$N(0,1)$$.
    * Để chuyển một phân phối chuẩn thành một phân phối chuẩn tắc ta sử dụng [Z-score](https://online.stat.psu.edu/stat500/sites/stat500/files/Z_Table.pdf).
    * Z của một giá trị chỉ ra giá trị tại 1 điểm chênh lệch với giá trị trung bình bao nhiêu độ lệch chuẩn.
    * $$Z =  \frac{(observed value - mean)}{SD}$$
    
# 5. Phân phối mẫu <a name="5"></a>
## 5.1. Phân phối mẫu của trung bình mẫu <a name="5_1"></a>
* Nếu population là một phân phối chuẩn với $$N(\mu,\sigma^{2})$$ thì trung bình mẫu cũng xấp xỉ là một phân phối chuẩn với $$N(\mu,\frac{\sigma}{\sqrt{n}})$$.
* Định lý giới hạn trung tâm: Nếu chúng ta lấy nhiều mẫu từ một population với cỡ mẫu lớn (n>30) thì trung bình mẫu cũng sẽ xấp xỉ tuân theo phân phối chuẩn với $$N(\mu,\frac{\sigma}{\sqrt{n}})$$, với $$\mu$$ và $$\sigma$$ là trung bình và độ lệch chuẩn của population.
## 5.2. Phân phối mẫu của tỉ lệ mẫu <a name="5_2"></a>
* Phân bố mẫu của tỉ lệ mẫu xấp xỉ là một phân bố chuẩn với $$N(p, \frac{p(1-p)}{n})$$ khi:
    * np \ge 5
    * n(1-p) \ge 5
        * Với:
            * n là số phần tử của sample.
            * p là tỉ lệ trên population.
* $$z = \frac{\hat{p} - p}{\sqrt{\frac{p(1-p)}{n}}}$$ tuân theo phân phối chuẩn tắc với $$\hat{p}$$ là tỉ lệ trên sample
# 6. Ước lượng và khoảng tin cậy <a name="6"></a>
* Dạng tổng quát của khoảng tin cậy: `sample statistic` $$ \pm $$ `margin of error`
* Dạng tổng quát của biên độ sai số: `margin of error` $$ = M * \hat{SE}$$ với M phụ thuộc vào độ tin cậy
## 6.1. Ước lượng cho tỉ lệ trên population <a name="6_1"></a>
* Ước lượng điểm cho tỉ lệ trên population p là: $$\hat{p}$$ 
* Nếu np và n(1-p) lớn hơn 15 thì $$\hat{p}$$ xấp xỉ là một phân bố chuẩn với mean = p và SE = $$\sqrt{\frac{p(1-p)}{n}}$$
* $$(1 - \alpha)100%$$% khoảng tin cậy cho tỉ lệ population p nằm giữa $$\hat{p} \pm z_{\alpha/2}\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}$$.
* Nếu muốn xác định n ta sẽ dùng 2 phương pháp sau đây:
    * Phương pháp Educated Guess: $$n = \frac{z_{\alpha /2}^2 \hat{p}_g (1-\hat{p}_g)} {E^2}$$
    * Phương pháp: Conservative: $$n = \frac{z_{\alpha /2}^2 (\frac{1}{2})^2}{E^2}$$
## 6.2. Ước lượng cho trung bình trên population <a name="6_2"></a>
* Ước lượng điểm cho trung bình trên population p là: $$\hat{x}$$
* Nếu population là một phân phối chuẩn hoặc cỡ mẫu là lớn (n>30) thì khi đó $$\frac{\hat{x}-\mu}{\frac{s}{\sqrt{n}}}$$ sẽ tuân theo phân phối t với bậc tự do = n-1
* $$(1 - \alpha)100$$% khoảng tin cậy trung bình population $$\mu$$ nằm giữa $$\hat{x} \pm t_{\alpha /2} \frac{s}{\sqrt{n}}$$
* Nếu muốn xác định n ta sẽ dùng 2 phương pháp sau đây:
    * Phương pháp cruder: $$n = (\frac{z_{\alpha / 2} s}{E})^2$$
    * Phương pháp iterative: $$n = (\frac{t_{\alpha / 2} \sigma}{E})^2$$
# 7. Kiểm định giả thuyết <a name="7"></a>
* Các khái niệm trong kiểm định giả thuyết:
    * Giả thuyết đối $$H_o$$: Là giả thuyết được chấp nhận cho đến khi tìm ra bằng chứng chống lại nó.
    * Giả thuyết mong muốn $$H_{\alpha}$$: Là giả thuyết mà chúng ta muốn kết luận 
    * Lỗi loại I: xảy ra khi ta bác bỏ giả thuyết $$H_o$$ khi nó đúng
    * Lỗi loại II: xảy ra khi ta không thể bác bỏ giả thuyết $$H_o$$ khi nó sai
    * $$\alpha$$: Xác suất vi phạm lỗi loại I
    * $$\beta$$: Xác suất vi phạm lỗi loại II
    * Power: Xác suất $$H_o$$ bị bác bỏ khi nó sai.
* 6 bước để kiểm định giả thuyết:
    * Xác định $$H_o$$, $$H_{\alpha}$$ và kiểm tra các điều kiện
    * Xác định $$\alpha$$
    * Tính toán thống kê thử nghiệm
    * Xác định p-value hoặc vùng bác bỏ
    * Quyết định bác bỏ hay không bác bỏ $$H_o$$
    * Kết luận
* Các giả thuyết có thể đưa ra trên 1 mẫu:
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-10-12_17-15-43.png)
## 7.1. Kiểm định tỉ lệ trên 1 mẫu <a name="7_1"></a>
* $$z^{*} = \frac{\hat{p} - p_o}{\sqrt{\frac{p_o(1-p_0)}{n}}}$$ là một giá trị được sử dụng để quyết định xem ta có thể bác bỏ hay không giả thuyết $$H_o$$ với 2 phương pháp sau đây:
    * Sử dụng miền tiêu chuẩn, bác bỏ $$H_o$$ khi:
        * $$z^{*} \le c_{\alpha}$$ với kiểm định bên trái
        * $$z^{*} \ge c_{1 - \alpha}$$ với kiểm định bên trái
        * $$z^{*} \ge |c_{\alpha /2}|$$ với kiểm định 2 bên
    * Sử dụng p-value:
        * $$p-value = P(Z \le z^{*}) < \alpha $$ với kiểm định bên trái
        * $$p-value = P(Z \ge z^{*}) < \alpha $$ với kiểm định bên trái
        * $$p-value = 2*P(Z \ge |z^{*}|) < \alpha $$ với kiểm định 2 bên 
## 7.2. Kiểm định trung bình trên 1 mẫu <a name="7_2"></a>
* $$t^{*} = \frac{\hat{x} - \mu_o}{\sqrt{\frac{s}{\sqrt{n}}}}$$ là một giá trị được sử dụng để quyết định xem ta có thể bác bỏ hay không giả thuyết $$H_o$$ với 2 phương pháp sau đây:
    * Sử dụng miền tiêu chuẩn, bác bỏ $$H_o$$ khi:
        * $$t^{*} \le t_{\alpha}$$ với kiểm định bên trái
        * $$t^{*} \ge t_{1 - \alpha}$$ với kiểm định bên trái
        * $$t^{*} \ge |t_{\alpha /2}|$$ với kiểm định 2 bên
    * Sử dụng p-value:
        * $$p-value = P(t \le t^{*}) < \alpha $$ với kiểm định bên trái
        * $$p-value = P(t \ge t^{*}) < \alpha $$ với kiểm định bên trái
        * $$p-value = 2*P(t \ge |t^{*}|) < \alpha $$ với kiểm định 2 bên
# 8. So sánh 2 parameter của population <a name="8"></a>
* Chúng ta có các so sánh sau:
    * Nếu các phép đo là định tính và được lấy từ 2 nhóm riêng biệt, thì phân tích sẽ liên quan đến việc so sánh 2 tỉ lệ mẫu độc lập
    * Nếu các phép đo là định lượng và được lấy từ 2 nhóm riêng biệt, thì phân tích sẽ liên quan đến việc so sánh 2 trung bình mẫu độc lập
    * Nếu các phép đo là định lượng và được lấy 2 lần từ mỗi đối tượng, thi phân tích sẽ liên quan đến việc so sánh 2 trung bình mẫu phụ thuộc
    * Nếu các phép đo là định tính và được lấy 2 lần từ mỗi đối tượng, thì phân tích sẽ liên quan đến việc so sánh 2 tỉ lệ mẫu phụ thuộc.
* Định lý hiệu của 2 biến độc lập có phân phối chuẩn:
    * Nếu X là 1 biến có phân phối chuẩn với trung bình $$\mu_x$$, phương sai $$\sigma_x^2$$ và độ lệch chuẩn $$\sigma_x$$
    * Nếu Y là 1 biến có phân phối chuẩn với trung bình $$\mu_y$$, phương sai $$\sigma_y^2$$ và độ lệch chuẩn $$\sigma_y$$
    * Với X và Y độc lập với nhau
    * Khi đó X - Y cũng tuân theo phân phối chuẩn với trung bình $$\mu_x - \mu_y$$, phương sai $$\sigma_x^2 + \sigma_y^2$$ và độ lệch chuẩn $$\sqrt{\sigma_x^2 + \sigma_y^2}$$
## 8.1. So sánh 2 tỉ lệ của population <a name="8_1"></a>
* Gọi $$n_1, p_1$$ lần lượt là kích thước và tỉ lệ của population 1
* Tương tự $$n_2, p_2$$ lần lượt là kích thước và tỉ lệ của population 2
* Population 1 và 2 độc lập với nhau
* Nếu $$n_1\hat{p_1}$$, $$n_1(1-\hat{p_1})$$, $$n_2\hat{p_2}$$, $$n_2(1-\hat{p_2})$$ đều lớn hơn 5
### 8.1.1. So sánh 2 tỉ lệ của population bằng khoảng tin cậy <a name="8_1_1"></a>
* Thì với $$(1-\alpha)100$$ % khoảng tin cậy thì ước lượng khoảng $$p_1 - p_2$$ là:$$ \hat{p_1} - \hat{p_2} \pm z_{\alpha /2}\sqrt{\frac{\hat{p_1}(1-\hat{p_1})}{n_1} + \frac{\hat{p_2}(1-\hat{p_2})}{n_2}}$$
### 8.1.2. So sánh 2 tỉ lệ của population bằng kiểm định giả thuyết <a name="8_1_2"></a>
* Giả thuyết đối: $$H_o: p_1 - p_2 = 0$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: p_1 - p_2 \ne 0 $$
    * $$ H_{\alpha}: p_1 - p_2 > 0 $$
    * $$ H_{\alpha}: p_1 - p_2 < 0 $$
* $$ z^* = \frac{ (\hat{p_1} - \hat{p_2}) }{ \sqrt{ \hat{p}^* (1-\hat{p}^*) (\frac{ 1 }{ n_1 ) } + \frac{ 1 }{ n_2 })} } $$, với $$\hat{p}^* = \frac{x_1 + x_2}{n_1 + n_2}$$ là một giá trị được sử dụng để quyết định xem ta có thể bác bỏ hay không giả thuyết $$H_o$$
* 2 population thì không phụ thuộc vào nhau, phân phối của mỗi population là phân phối chuẩn hoặc có một cỡ mẫu lớn
## 8.2. So sánh 2 trung bình của population <a name="8_2"></a>
### 8.2.1. Không phụ thuộc nhau <a name="8_2_1"></a>
* Gọi $$n_1, s_1$$ lần lượt là kích thước mẫu và độ lệch chuẩn mẫu trên population 1
* Gọi $$n_2, s_2$$ lần lượt là kích thước mẫu và độ lệch chuẩn mẫu trên population 2
#### 8.2.1.1. 2 population có cùng phương sai <a name="8_2_1_1"></a>
* Chúng ta có thể chấp nhận 2 population có cùng phương sai khi tỉ lệ độ lệch chuẩn mẫu trên 2 population từ 0.5 đến 2.
* Khi đó độ lệch chuẩn chung được ước lượng được như sau: $$ s_p = \sqrt{ \frac{ (n_1 - 1) s_1^2 + (n_2 - 1) s_2^2 }{ n_1 + n_2 - 2 } } $$
* Khi chúng ta giả sử  và phương sai của 2 population bằng nhau thì ta có thể nói rằng: $$ t = \frac{ \overline{x_1} - \overline{x_2} }{ s_p \sqrt{ \frac{ 1 }{ n_1 } + \frac{ 1 }{ n_2 } } } $$ tuân theo phân phối t với $$ n_1 + n_2 - 2 $$ bậc tự do
##### 8.2.1.1.1. Khoảng tin cậy <a name="8_2_1_1_1"></a>
* Khi đó, ta có $$ (1 - \alpha) 100 $$ % khoảng tin cậy cho $$ \mu_1 - \mu_2 $$ là: $$ \mu_1 - \mu_2 \pm t_{\alpha / 2} \sqrt{ \frac{ 1 }{ n_1 } + \frac{ 1 }{ n_2 } }$$ với $$ t_{\alpha /2} $$ đến từ một phân phối t với $$ n_1 + n_2 - 2 $$ độ lệch chuẩn.
##### 8.2.1.1.2. Kiểm định giả thuyết <a name="8_2_1_1_2"></a>
* Giả thuyết đối $$H_o: \mu_1 - \mu_2 = 0$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: \mu_1 - \mu_2 \ne 0 $$
    * $$ H_{\alpha}: \mu_1 - \mu_2 > 0 $$
    * $$ H_{\alpha}: \mu_1 - \mu_2 < 0 $$
* $$ t^* = \frac{ \hat{x_1} - \hat{x_2} }{ s_p \sqrt{ \frac{ 1 }{ n_1 } + \frac{ 1 }{ n_2 } } } $$ tuân theo phân phối t với $$ n_1 + n_2 - 2 $$ bậc tự do
#### 8.2.1.2. 2 population không cùng phương sai <a name="8_2_1_2"></a>
* $$ t* = \frac{ \hat{x_1} - \hat{x_2} }{ s_p \sqrt{ \frac{ 1 }{ n_1 } + \frac{ 1 }{ n_2 } } } $$ tuân theo phân phối t với bậc tự do $$ df = \frac{ (n_1 - 1)(n_2 - 1) }{ (n_2 - 1) C^2 + (1 - C)^2 (n_1 - 1) } $$ với $$ C = \frac{ \frac{ s_1^2 }{ n_1 } }{ \frac{ s_1^2 }{ n_1 } + \frac{ s_2^2 }{ n_2 } } $$ 
* $$ (1 - \alpha) 100 $$ % khoảng tin cậy cho $$ \mu_1 - \mu_2 $$ là: $$ \overline{x}_1 - 
\overline{x}_2 \pm t_{\alpha /2} \sqrt{\frac{s_1^2}{n_1} + \frac{s_2^2}{n_2} $$ với $$t_{\alpha /2}$$ đến từ phân phối t với bậc tự do $$df$$ như trên
### 8.2.2. Phụ thuộc nhau <a name="8_2_2"></a>
* $$x_1, x_2,...,x_n$$ và $$y_1, y_2,...,y_n$$
* $$ d_1 = x_1 - y_1, d_2 = x_2 - y_2,..., d_n = x_n - y_n $$
* Trung bình mẫu của sự khác biệt là: $$\overline{d} = \frac{ 1 }{ n } \sum_{i = 1}^{n} d_i$$
* Độ lệch chuẩn mẫu của sự khác biệt là: $$s_d$$
* Nếu $$\overline{d}$$ là phân phối chuẩn hoặc cỡ mẫu lớn thì phân phối mẫu của $$\overline{d}$$ xấp xỉ là phân phối chuẩn với trung bình $$\mu_d$$, lỗi tiêu chuẩn $$\frac{\sigma_d}{\sqrt{n}}$$ và độ lệch chuẩn $$\frac{s_d}{\sqrt{n}}$$
#### 8.2.2.1. Khoảng tin cậy <a name="8_2_2_1"></a>
* Khi đó ta có $$ (1 - \alpha) 100 $$ % khoảng tin cậy cho $$\mu_d$$ là: $$\overline{d} \pm t_{\alpha /2} \frac{s_d}{\sqrt{n}}$$ với $$t_{\alpha /2}$$ đến từ phân phối t với $$n-1$$ bậc tự do
#### 8.2.2.2. Kiểm định giả thuyết <a name="8_2_2_2"></a>
* Giả thuyết đối: $$H_o: \mu_d = 0$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: \mu_d \ne 0 $$
    * $$ H_{\alpha}: \mu_d > 0 $$
    * $$ H_{\alpha}: \mu_d < 0 $$
* $$ t^* = \frac{\overline{d} }{ \frac{s_d}{\sqrt{n}} } $$ tuân theo phân phối t với $$ n - 1 $$ bậc tự do
## 8.3. So sánh 2 độ lệch chuẩn của population <a name="8_3"></a>
* Giả thuyết đối: $$H_o : \frac{\sigma_1^2}{\sigma_2^2} = 1$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: \frac{\sigma_1^2}{\sigma_2^2} \ne 1 $$
    * $$ H_{\alpha}: \frac{\sigma_1^2}{\sigma_2^2} > 1 $$
    * $$ H_{\alpha}: \frac{\sigma_1^2}{\sigma_2^2} < 1 $$
* Có 3 phương pháp khác nhau để kiểm định:
    * F-test: Sử dụng khi 2 mẫu được lấy từ 2 population có phân phối chuẩn
    * Bonett's test: Chỉ cần 2 mẫu định lượng
    * Levene's test: Chỉ cần 2 mẫu định lượng, sử dụng tốt khi một trong 2 mẫu bị skewed và 2 cỡ mẫu đều dưới 20.
# 9. Kiểm định Chi-Square cho những biến độc lập <a name="9"></a>
* Giá thuyết đối: $$H_o:$$ Trong population, 2 biến phân loại là độc lập
* Giá thuyết mong đợi: $$H_{\alpha}:$$ Trong population, 2 biến phân loại là phụ thuộc
* Trong bảng tổng hợp, có $$r$$ x $$c$$ ô với r là số hàng và c là số cột. Đặt $$O_1, O_2,...,O_{rc}$$ là giá trị quan sát được ở các ô, $$E_1, E_2,...,E_{rc}$$ là giá trị mong đợi ở các ô, với điều kiện 2 biến là độc lập, ta có: $$X^{2*} = \sum_{i=1}^{rc}{\frac{(O_i - E_i)^2}{E_i}}$$ 
* p-value được tính bằng $$P(X^2>X^{2*})$$ với bậc tự do là $$(r-1)(c-1)$$, với điều kiện có phải có hơn 20% ô có giá trị lớn hơn 5.
* Rủi ro là xác suất một sự kiện sẽ xảy ra
* Rủi ro tương đối là tỉ lệ rủi ra giữa 2 nhóm
* Rủi ro cơ sở là mẫu số của rủi ro tương đối
* Tỉ lệ cược là tỉ lệ số lần thành công chia cho số lần thất bại
# 10. Hồi quy tuyến tính <a name="10"></a>
* Hệ số tương quan: $$r = \frac{ \sum(x_i - \overline{x})(y_i - \overline{y}) }{ \sqrt{ \sum(x_i - \overline{x})^2 } \sqrt{\sum(y_i - \overline{y})^2} }$$
## 10.1. Mô hình hồi quy tuyến tính đơn giản <a name="10_1"></a>
* Có 4 điều kiện cần phải kiểm tra trước khi sử dụng mô hình hồi quy tuyến tính đơn giản:
    * Tuyến tính: Các giá trị hầu như phải phân phối trên 1 đường thẳng
    * Độc lập sai số: Không có một mối quan hệ nào giữa biến trả về và sai số
    * Mức độ bình thường của sai số: Các sai số hầu như phải nằm trên 1 đường thẳng
    * Phương sai bằng nhau: Phương sai của csac sai số gần như là bằng nhau ở mọi giá trị của biến giải thích
* Dạng tổng quát của mô hình hồi quy tuyến tính đơn giản là: $$Y = \beta_o + \beta_1 X + \epsilon$$
* Đối với 1 điểm dữ liệu: $$y_i = \beta_o + \beta_1 x_i + \epsilon_i$$. Trong đó: 
    * $$\beta_o$$ là tung độ gốc
    * $$\beta_1$$ là hệ số góc
    * $$\epsilon_i$$ là sai số hoặc độ lệch của $$y_i$$ từ đường $$\beta_o + \beta_1 x_i$$
* Đường bình phương nhỏ nhất là đường có tổng sai số bình phương của các dự đoán cho tất cả các điểm mẫu là ít nhất.
* Sử dụng phương pháp bình phương nhỏ nhất, chúng ta có thể tìm ước lượng cho hai tham số
* Các công thức để tính ước lượng bình phương nhỏ nhất là:
    * Hệ số góc của mẫu: $$\hat{\beta}_1 = \frac{\sum(x_i - \overline{x})(y_i - \overline{y})}{\sum(x_i - \overline{x})^2}$$
    * Tung độ gốc của mẫu: $$\hat{\beta}_o = \overline{y} - \hat{\beta}_i \overline{x}$$
* Phương trình hồi quy bình phương nhỏ nhất: $$\hat{y} = \hat{\beta}_o + \hat{\beta}_1 x$$
* Chúng ta cũng có thể sử dụng đường hồi quy bình phương nhỏ nhất để ước tính sai số $$\hat{\epsilon} = y_i - \hat{y}_i$$ là sai số của mỗi điểm dữ liệu
* Ước lượng hệ số góc:
    * Kiểm định giả thuyết: 
<table>
  <thead>  
   <tr> 
    <th>Câu hỏi nghiên cứu</th>
    <th>Có mối quan hệ tuyến tính không</th>
    <th>Có mối quan hệ tuyến tính cùng hướng không</th>
    <th>Có mối quan hệ tuyến tính ngược hướng không không</th> </tr>  
  </thead>  
  <tbody>  
    <tr>
      <th>Giả thuyết đối</th>
      <td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%3D%200" alt="\beta_1 = 0" /></p> </td>
      <td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%3D%200" alt="\beta_1 = 0" /></p> </td>
<td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%3D%200" alt="\beta_1 = 0" /></p> </td> 
    </tr>  
    <tr>
      <th>Giả thuyết mong muốn</th>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%5Cne%200" alt="\beta_1 \ne 0" /></p></td>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%3E%200" alt="\beta_1 &gt; 0" /></p></td>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cbeta_1%20%3C%200" alt="\beta_1 &lt; 0" /></p></td>
    </tr>  
    <tr>  
      <th>Loại kiểm định</th>
      <td>Hai bên, không định hướng</td>  
      <td>Bên phải, định hướng</td>
      <td>Bên trái, định hướng</td>
    </tr>    
  </tbody>  
</table> 
    
  * $$t^* = \frac{\hat{\beta}_1}{\hat{SE} (\hat{\beta}_1)}$$ tuân theo phân phối t với n-2 bậc tự do, với $$\hat{SE} (\hat{\beta}_1)$$ là ước tính sai số chuẩn mẫu của hệ số góc

  * Tương tự, ta có $$(1-\alpha) 100 \%$$ khoảng tin cậy cho hệ số góc của population $$\beta_1$$ là: $$\hat{\beta}_1 \pm t_{\alpha /2} (\hat{SE} (\hat{\beta}_1)) $$ với t có n - 2 bậc tự do
## 10.2. Hệ số xác định <a name="10_2"></a>
* Hệ số xác định phần trăm biến thiên trong các giá trị y có thể được giải thích bằng mô hình hồi quy tuyến tính: $$R^2 = \frac{ \sum(\hat{y}_i - \overline{y})^2 }{ \sum(y_i - \overline{y}_i)^2  }$$
## 10.3. Suy luận tương quan <a name="10_3"></a>
* Điều kiện:
    * Không có bất kì oulier nào ở một trong 2 biến định lượng
    * Hai biến phải tuân theo phân phối chuẩn
* Giả thuyết đối: $$H_o: \rho = 0$$
* Giả thuyết mong muốn: $$H_{\alpha}: \rho \ne 0$$
<table>
  <thead>  
   <tr> 
    <th>Câu hỏi nghiên cứu</th>
    <th>Có mối quan hệ tuyến tính không</th>
    <th>Có mối quan hệ tuyến tính cùng hướng không</th>
    <th>Có mối quan hệ tuyến tính ngược hướng không không</th> </tr>  
  </thead>  
  <tbody>  
    <tr>
      <th>Giả thuyết đối</th>
      <td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%3D%200" alt="\rho = 0" /></p> </td>
      <td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%3D%200" alt="\rho = 0" /></p> </td>
      <td> <p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%3D%200" alt="\rho = 0" /></p> </td> 
    </tr>  
    <tr>
      <th>Giả thuyết mong muốn</th>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%5Cne%200" alt="\rho \ne 0" /></p></td>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%3E%200" alt="\rho &gt; 0" /></p></td>
      <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Crho%20%3C%200" alt="\rho &lt; 0" /></p></td>
    </tr>  
    <tr>  
      <th>Loại kiểm định</th>
      <td>Hai bên, không định hướng</td>  
      <td>Bên phải, định hướng</td>
      <td>Bên trái, định hướng</td>
    </tr>    
  </tbody> 
</table>
 
* $$t^* = \frac{r \sqrt{n-2}}{\sqrt{1 - r^2}}$$ tuân theo phân phối t với n - 2 bậc tự do
## 10.4. Mô hình hồi quy tuyến tính đa biến <a name="10_4"></a>
* $$Y = \beta_o + \beta_1 X_1 + ... + \beta_k X_k + \epsilon$$, với:
    * Y là biến trả về
    * $$X_1,...,X_k$$ là những biến giải thích
    * $$\beta_o, \beta_1,...,\beta_k$$ là những hằng số
    * $$\epsilon$$ là một biến ngẫu nhiên tuân theo phân phối chuẩn với trung bình bằng 0 và có phương sai $$\sigma_{\epsilon}^2$$
* Kiểm định F:
    * Giả thuyết đối: $$H_o: \beta_1 =...=\beta_k = 0$$
    * Giả thuyết mong muốn: $$H_{\alpha}:$$ Ít nhất một $$\beta_i \ne 0$$
* Với $$F*$$ tuân theo phân phối F
# 11. Giới thiệu về ANOVA <a name="11"></a>
## 11.1. Kiểm định giả thuyết một chiều <a name="11_1"></a>
* Điều kiện:
    * Biến trả về cho mỗi factor phải có phân phối chuẩn
    * Những phân phối có cùng phương sai
    * Dữ liệu độc lập
* Giả thuyết đối $$H_o: \mu_1 = \mu_2 = ... = \mu_t$$
* Giả thuyết mong muốn: $$H_{\alpha}: \mu_i \ne \mu_j$$, với một vài i và j khác nhau
* $$F = \frac{between-group-variance}{within-group-variance}$$, nếu tỉ lệ này lớn, thì chúng ta có đủ bằng chứng để bác bỏ giả thuyết đối
* Bảng ANOVA:
<table>
 <thead>
  <th>Source</th>
  <th>Bậc dự do</th>
  <th>SS</th>
  <th>MS</th>
  <th>F</th>
  <th>P-value</th>
 </thead>
 <tbody>
  <tr>
   <th>Treatment</th>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/t%20-%201" alt="t - 1" /></p></td>
   <td>
<p align="center"><img align="center" src="https://i.upmath.me/svg/SST" alt="SST" /></p></td>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/MST%20%3D%20%5Cfrac%7BSST%7D%7Bt-1%7D" alt="MST = \frac{SST}{t-1}" /></p></td>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/%5Cfrac%7BMST%7D%7BMSE%7D" alt="\frac{MST}{MSE}" /></p>
   </td>
  </tr>
  <tr>
   <th>Error</th>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/n_T%20-%20T%20" alt="n_T - T " /></p></td>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/SSE" alt="SSE" /></p></td>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/MSE%20%3D%20%5Cfrac%7BSSE%7D%7Bn_T-t%7D" alt="MSE = \frac{SSE}{n_T-t}" /></p></td>
   
  </tr>
  <tr>
   <th>Tổng</th>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/n_T%20-1%20" alt="n_T -1 " /></p></td>
   <td><p align="center"><img align="center" src="https://i.upmath.me/svg/TSS" alt="TSS" /></p></td>
  </tr>
 </tbody>
</table>

## 11.2. Kiểm định nhiều so sánh <a name="11_2"></a>
* Nếu kiểm định 1 chiều mà bác bỏ được giả thuyết đối thì ta cần kiểm tra xem sự khác biệt bên trong thật sự là gì bằng phương pháp Tukey
# 12. Giới thiệu về kiểm định phi tham số và Bootstrap <a name="12"></a>
## 12.1. Kiểm định Sign <a name="12_1"></a>
* Khi muốn kiểm định median thay vì mean ta sẽ dùng kiểm định Sign
* Điều kiện:
    * Biến random được quan tâm là biến liên tục
* Giả thuyết đối $$H_o: \eta = \eta_o$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: \eta \ne 1 $$
    * $$ H_{\alpha}: \eta > 1 $$
    * $$ H_{\alpha}: \eta < 1 $$
* $$S^+ = $$ tổng số quan sát lớn hơn, bé hơn hoặc khác với $$\eta$$ (tùy thuộc vào giả thuyết mong muốn) tuân theo phân phối nhị thức với p=0.5
## 12.2. Kiểm định Wilcoxon một mẫu <a name="12_2"></a>
* Khi muốn kiểm định median thay vì mean ta cũng có thể dùng kiểm định Sign
* Điều kiện:
    * Biến random được quan tâm là biến liên tục
    * Phân phối xác suất của population là đối xứng
* Giả thuyết đối $$H_o: \eta = \eta_o$$
* Các giả thuyết mong muốn:
    * $$ H_{\alpha}: \eta \ne 1 $$
    * $$ H_{\alpha}: \eta > 1 $$
    * $$ H_{\alpha}: \eta < 1 $$
* [W](https://sixsigmastudyguide.com/1-sample-wilcoxon-non-parametric-hypothesis-test/) là kiểm định thống kê của Wilcoxon
## 12.3. Phương pháp lấy mẫu Bootstrapping <a name="12_3"></a>
* Phương pháp gồm 4 bước:
    * Đặt sample là population
    * Lấy mẫu có hoàn lại từ population nhiều lần, tốt nhất là trên 1000 lần
    * Tính toán các đại lượng mong muốn từ các mẫu vừa lấy (mean, median, ...)
    * Sử dụng các ước lượng thống kê của Bootstrap sampling đã tính vừa rồi để đánh giá độ chính xác các ước lượng thống kê của mẫu ban đầu (Original sample, Training Data)
* Độ tin cậy: 100(1-2α)
# 13. Tóm tắt các kỹ thuật thống kê <a name="13"></a>
* Ước lượng một giá trị:
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-10-15_12-12-30.png)
* Kiểm định một giả thuyết:
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-10-15_12-14-36.png)
* Kiểm tra một mối quan hệ:
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-10-15_12-16-14.png)
* [Chi tiết tất cả các kỹ thuật thống kê](https://online.stat.psu.edu/stat500/sites/stat500/files/500%20L12-Table%20of%20Statistical%20Techniques.pdf)
