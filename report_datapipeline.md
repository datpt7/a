# DATA PIPELINE

## MỤC LỤC
1. [Kiến thức](#kien_thuc)
    1. [Đã học được](#kien_thuc_da_hoc_duoc)
        1. [Hadoop](#hadoop)
        2. [Spark](#spark)
        3. [Luigi](#luigi)
    2. [Còn vướng mắt](#kien_thuc_con_vuong_mat)

2. [Thực hành](#thuc_hanh)
    1. [Đã hoàn thành](#thuc_hanh_da_hoan_thanh)
    2. [Còn vướng mắt](#thuc_hanh_con_vuong_mat)
    3. [ Cách cài đặt và sử dụng Data Pipeline đã dựng trên server hoặc một máy tính khác](#cai_dat_datapipeline)
        1. [Cài đặt](#cai_dat)
        2. [Sử dụng](#su_dung)

## NỘI DUNG
## 1. Kiến thức <a name="kien_thuc"></a>
### 1.1. Đã học được <a name="kien_thuc_da_hoc_duoc"></a>
#### 1.1.1. Hadoop <a name="hadoop"></a>
##### 1.1.1.1 HDFS
* HDFS là hệ thống lưu trữ file phân tán của Hadoop. 
* HDFS sử dụng kiến trúc master/slave:
    * Master gồm 1 node được gọi là NameNode để quản lý metadata, metadata có thể bao gồm tên của file, kích thước, thông tin về block. Đồng thời cũng điều phối các tác vụ xóa, tạo, nhân bản,... các block cho DataNode.
    * Slave gồm 1 hoặc nhiều node, các node này được gọi là DataNode để lưu trữ dữ liệu.
* Một file được lưu trữ trên HDFS sẽ được chia nhỏ thành các block với kích thước mặc định là 128MB (có thể thay đổi được), mỗi block sẽ được nhân bản để lưu trên nhiều DataNode. Chính điều này giúp HDFS có khả năng chịu lỗi và tính sẵn sàng cao.
* Rack là tập hợp các DataNode sử dụng cùng 1 switch (thường là khoảng 40 đến 50 DataNode). Hadoop tin rằng, các DataNode trên cùng 1 Rack sẽ giao tiếp với nhau tốt hơn là khác Rack.
* Thuật toán Rack Awareness quy định:
    * Không có quá 1 nhân bản của 1 block trên cùng 1 DataNode.
    * Không có quá 2 nhân bản của 1 block trên cùng 1 Rack.
* Thuật toán Rack Awareness giúp NameNode có thể chọn được DataNode gần nhất giúp đạt được hiệu suất tối đa khi đọc và ghi dữ liệu.

##### 1.1.1.2 YARN
* YARN là một framework cung cấp các daemon và API cần thiết giúp phát triển ứng dụng phân tán.
* YARN chịu trách nhiêm xử lý và lập lịch sử dụng tài nguyên tính toán (CPU hay memory) cũng như giám sát quá trình thực thi các ứng dụng đó.
* YARN có 2 trình xử lý:
    * ResourceManager: quản lý toàn bộ tài nguyên tính toán của cụm.
    * NodeManager: giảm sát việc sử dụng tài nguyên (CPU, memory, disk, network,...) của container và báo cáo với ResourceManager.
* ResourceManager có hai thành phần quan trọng:
    * Scheduler: Có trách nhiệm phân bổ tài nguyên cho các ứng dụng khác nhau.
    * ApplicationManager: Có chức năng:
        * Nhận một job được submit
        * Tìm 1 container hợp lý để thực thi ApplicationMaster.
        * Khởi động lại container ApplicationMaster khi không thành công.

#### 1.1.2. Spark <a name="spark"></a>
##### 1.1.2.1. RDD
* RDD là một kiểu dữ liệu cơ bản của Spark.
* Là một tập hợp Immutability phân tán.
* Được chia thành các partition lưu trữ trên các worker để xử lý song song.
* Được lưu trên Ram, do đó tăng tốc độ xử lý lên 10-100 lần so với Hadoop MapReduce.
* Spark cung cấp các transformation và action để thao tác với RDD (hoặc các kiểu dữ liệu khác như Data Frame, Data Set ,... Nhưng tất cả các loại này đều sẽ được chuyển thành RDD vì RDD là kiểu dữ liệu cơ bản của Spark).

##### 1.1.2.1. Transformation
* Transformation xử lý theo kiểu lazily (dựng các execution plans).
* Một số transformaition thường dùng:
    * `distinct`: loại bỏ trùng lặp trong RDD.
    * `filter`: dùng để lọc dữ liệu, có thể sử dụng lambda kết hợp với filter.
    * `map`: dùng để chuyển đổi một RDD thành một RDD bằng cách xử lý mỗi phần tử của RDD.
    * `flatMap`: cũng dùng để chuyển đổi một RDD thành một RDD khác như map, nhưng mỗi phần tử của RDD sẽ được chuyển hoặc không chuyển thành nhiều phần tử mới. 
    * `sortBy`: mô tả một hàm để trích xuất dữ liệu từ các object của RDD vầ thực hiện sort.
    * `randomSplit`: nhận một mảng các trọng số va tạo một random seed, tách các RDD thành các mảng RDD có số lượng chia theo trọng số.
* Nói 1 cách đơn giản thì transformation sẽ chuyển từ 1 RDD này sang một RDD khác.

##### 1.1.2.2. Action
* Các transformation chỉ thực hiện khi 1 action được gọi sau đó.
* Một số action thường dùng:
    * `reduce`: thực hiện hàm reduce trên RDD để thu về 1 giá trị duy nhất.
    * `count`: đếm số dòng trong RDD.
    * `countApprox`: đếm xấp xỉ số dòng trong RDD, nhưng phải cung cấp timeout vì có thể không nhận được kết quả.
    * `fist`: lấy giá trị đầu tiên của RDD.
    * `take`: lấy một lượng giá trị từ RDD.

##### 1.1.2.3. Driver
* Khái niệm:
    * Driver là một Java process.
    * Process thực thi được các chương trình viết bằng Scala, Java, Python.
    * Thực thi code và tạo một SparkSession hoặc SparkContext.
    * SparkSession có trách nhiệm tạo các DataFrame, DataSet, RDD, truy vấn SQL, thực hiện Transformation và Action,...
* Chịu trách nhiệm:
    * Hàm main của chương trình được chạy trong một Driver process: tạo SparkSession hoặc SparkContext.
    * Chuyển code của chương trình thành các Task (Transformation và Action) và xác định số Task.
    * Giúp tạo Lineage, Logical Plan và Physical Plan.
        > **_Lineage:_** Khi một RDD mới được tạo từ một RDD cha thông qua Transformatoin thì RDD mới đó sẽ chứa một con trỏ, trỏ đến RDD cha. Spark sẽ theo dõi mối quan hệ giữa các RDD này bằng cách sử dụng một thành phần được gọi là Linage. Khi một RDD bị mất dữ liệu thì Linage sẽ có nhiệm vụ khôi phục lại dữ liệu.
        
        > **_Logical Plan:_** Là kế hoạch mô tả những đầu ra mong đợi sau khi áp dụng một loạt các Transformation như join, filter, where, groupBy,...

        > **_Physical Plan:_** Là kế hoạch quyết định loại join và trình tự thực hiện các lệnh filter, where, groupBy, ...
    * Khi một Physical Plan được tạo, Driver sẽ kết hợp với Cluster Manager lập lịch thực thi những Task.
    * Theo dõi metadata ở các Executor.
* Memory của driver thường được đặt bằng với executor, số core mặc định là 1.

##### 1.1.2.4. Worker
* Worker là thành phần của một cụm Spark cụ thể, nơi mà các executor sống để thực thi các task. Người ta thường gọi là các node tính toán trong Spark.
* Mỗi worker có thể chứa nhiều executor nếu nó có đủ memory và core. 
* Worker chia ram và core được NodeManager yêu cầu cho các executor thực thi bên trong đó.
* Nếu một worker gặp vấn đề thì những task được thực thi bởi các executor bên trong worker đó sẽ được chuyển sang các executor ở các worker khác.

##### 1.1.2.5. Executor
* Mỗi executor sẽ được dùng lượng memory và core cho phép để thực hiện các task.
* Số lượng core trong mỗi executor nên đặt trong khoảng từ 2 đến 5. Vì nếu quá nhiều core dẫn đến việc có quá nhiều request đọc dữ liệu từ HDFS, làm giảm hiệu suất của executor. Hoặc nếu chỉ có 1 executor thì không tận dụng được ưu điểm xử lý đa luồng của JVM.
* Executor thực thi các task được giao và trả kết quả về cho driver thông qua cluster manager.

##### 1.1.2.6. Applications, tasks, stages, jobs
* Application là một chương trình tính toán dùng để chạy code do người dùng cung cấp. Nó bao gồm driver và các executor.
* Job là một công việc tính toán song song bao gồm nhiều Task. Một job được sinh ra khi có 1 action được gọi trong code.
* Mỗi job sẽ được chia thành nhiều Stage. Một Stage được sinh ra nếu gặp wide transformation (ví dụ: reduceByKey,...)
* Mỗi stage gồm một nhóm các Task, mỗi task trên mỗi partition.

##### 1.1.2.7. Sự khác biệt cơ bản của các loại mode khi submit 1 Spark Job lên YARN
###### 1.1.2.7.1. YARN: Client-mode
* Vì là Client mode nên chắc chắn Client vẫn sẽ phải kết nối liên tục với Application Master trong suốt quá trình thực thi để nhập Input cũng như xem Output.
* Driver sẽ được chạy ngay bên trong client.
* Phù hợp cho việc debug.
###### 1.1.2.7.2. YARN: Cluster-mode
* Vì là Cluster mode nên Client sẽ không cần phải kết nối liên tục như Client mode.
* Driver sẽ được chạy ngay bên trong Application Master. Do đó lúc này Application sẽ phải làm 2 việc cùng 1 lúc đó chính là yêu cầu tài nguyên từ YARN và công việc của driver.
* Phù hợp cho production.

### 1.2. Còn vướng mắt, chưa hiểu <a name="kien_thuc_con_vuong_mat"></a>

## 2. Thực hành <a name="thuc_hanh"></a>
### 2.1. Đã hoàn thành <a name="thuc_hanh_da_hoan_thanh"></a>
* Dùng docker build 1 cluster bao gồm Hdfs, Yarn và Spark.
* Code python kết hợp với crontab tự động sinh data theo giờ với định dạng csv và đẩy lên hdfs.
* Code Scala tạo một Spark Job xử lý dữ liệu trên hdfs và ghi xuống redis database.
* Code Python dùng package Luigi kết hợp với crontab tự động thực hiện một data pipeline theo giờ hiện tại: Kiểm tra folder dữ liệu của giờ hiện tại sau đó submit Spark Job.

### 2.2. Những vướng mắt hiện tại và hướng giải quyết  <a name="thuc_hanh_con_vuong_mat"></a>
* Vướng mắt: Liệu có thể gộp Spark Job, Crontab và Luigi vào 1 container độc lập sau đó submit lên cluster đã tồn tại với master là yarn.
* Hướng giải quyết: Tìm hiểu lại cách thức submit một job lên yarn có thật sự yêu cầu client phải nằm trong sự quản lý của yarn hay không.

### 2.3. Cách cài đặt và sử dụng Data Pipeline đã dựng trên server hoặc một máy tính khác <a name="cai_dat_datapipeline"></a>
#### 2.3.1. Cài đặt <a name="cai_dat"></a>
* Cài đặt git [xem hướng dẫn](https://www.atlassian.com/git/tutorials/install-git).
* Cài đặt docker [xem hướng dẫn](https://docs.docker.com/engine/install/).
* Pull project về máy: `git clone https://gitlab.com/datpt7/practice_tests.git`.
* Tạo một mạng cho các cluster: `docker network create --driver bridge hadoop-network --subnet=172.16.0.0/16`.

#### 2.3.2. Sử dụng <a name="su_dung"></a>
* Di chuyển vào folder docker: `cd practice_tests/docker`.
* Build docker-compose: `docker-compose build`.
    > **_Chú ý:_** Nếu bạn đã thay đổi code theo ý của mình trước khi build (spark job jar, luigi, crontab), cần phải chuyển các file mới nhất về Context của docker. Cấp quyền thực thi cho file moveFile.sh, sau đó chạy moveFile.sh để chuyển các file mới nhất về Context trước khi build.
* Up docker-compose: `docker-compose up -d`.

