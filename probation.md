# Backend, Frontend, System (Confidential)

## 1. Gearman và RabbitMQ

### 1.1. Gearman
* Gearman cung cấp một framework ứng dụng phân tán để làm việc với nhiều máy hoặc nhiều quy trình. Nó cho phép các ứng dụng hoàn thành các tác vụ song song, xử lý cân bằng tải và gọi các chức năng giữa các ngôn ngữ. Framework có thể được sử dụng trong nhiều ứng dụng khác nhau.
* Gearman ban đầu được viết bằng Perl, nhưng sau này được đổi thành C để tăng hiệu suất.

### 1.2. RabbitMQ
* RabbitMQ is a message-queueing software also known as a message broker or queue manager. Simply said; it is software where queues are defined, to which applications connect in order to transfer a message or messages.
* RabbitMQ là một phần mềmmessage-queueing. Nói đơn giản; nó là phần mềm trung gian để các ứng dụng khác nhau có thể gửi và nhận tin nhắn.

### 1.3. So sánh Gearman và RabbitMQ

## 2. Nginx và Jetty
### 2.1. Nginx

* Khái niệm: Nginx là một máy chủ web mã nguồn mở, ngoài ra nó cũng được sử dụng làm reverse proxy, cache HTTP, load balancer,... Tuy nhiên, hiện tại nginx được ứng dụng làm reverse proxy nhiều hơn.
* Ưu điểm:
   * Code nhất quán và dễ hiểu hơn những đối thủ khác.
   * Dễ cấu hình.
   * Sử dụng ít bộ nhớ và resource.
   * Giúp website hoạt động nhanh hơn.
   * Có khả năng tương tích với nhiều ứng dụng web đang nổi trội ở hiện tại như ruby, python, Joomla
   * The written code base is more consistent than other alternatives.
   * Có thể xử lý hàng ngàn connection một lúc.
* Nhược điểm:
   * Cộng đồng còn nhỏ so với Apache.
   * Không cung cấp cho bạn nhiều module và extension so với Apache

## 2.2. Jetty
* Khái niệm: Jetty là một Java web server, như là servlet container, cung cấp ứng dụng với các tính năng cần thiết để tạo và chạy một servlet hoặc API.
* Ưu điểm: 
   * Nhẹ
   * Nhanh
   * Có thể nhúng
   * Scale tốt
* Nhược điểm:
   * Có các vấn đề liên quan đến thời gian bootup
   * Người dùng yêu cầu một chút kiến ​​thức để sử dụng nó một cách dễ dàng.

### 2.3. So sánh Nginx và Jetty

## 3. Elastic Search
* Khái niệm: Elasticsearch là một công cụ tìm kiếm với nhiều REST API đơn giản có khả năng phân tích và lưu trữ tất cả các dữ liệu có dạng như textextual, digital, geospatial, structured và unstructured một cách nhanh chóng (near real-time)
* Chi tiết: 
   * Là một search engine.
   * Được kế thừa từ Lucene Apache
   * Hoạt động như 1 web server, có khả năng tìm kiếm nhanh chóng (near realtime) thông qua giao thức RESTful
   * Có khả năng phân tích và thống kê dữ liệu
   * Chạy trên server riêng và đồng thời giao tiếp thông qua RESTful do vậy nên nó không phụ thuộc vào client viết bằng gì hay hệ thống hiện tại của bạn viết bằng gì. Nên việc tích hợp nó vào hệ thống là dễ dàng, chỉ cần gửi request http lên là nó trả về kết quả.
   * Là 1 hệ thống phân tán và có khả năng mở rộng tuyệt vời (horizontal scalability). Lắp thêm node cho nó là nó tự động auto mở rộng.
* Thành phần:
   * Document: là một JSON object với một số dữ liệu và là đơn vị nhỏ nhất để lưu dữ liệu trong ES.
   * Index: được thiết kế để cho phép tìm kiếm full-text search. Nó tách văn bản ra thành từng từ có nghĩa sau đó sẽ map xem thuộc văn bản nào. Khi search tùy thuộc vào loại search sẽ đưa ra kết quả cụ thể.
   * Shard: là đối tượng của Lucene , là tập con các documents của 1 Index. Một Index có thể được chia thành nhiều shard. Bao gồm 2 loại: primary shard và replica shard.
   * Node: là trung tâm hoạt động của Elasticsearch. Là nơi lưu trữ dữ liễu ,tham gia thực hiện đánh index cúa cluster cũng như thực hiện các thao tác tìm kiếm. Mỗi node được định danh bằng 1 unique name.
   * Cluster: là tập hợp các nodes hoạt động cùng với nhau, chia sẽ cùng thuộc tính cluster.name. Nó có một node chính và có thể có nhiều node phụ.

## 4. Web và protocol
### 4.1. Http protocol
* Vòng đời của http request:
   * Tiền xử lý phía client.
   * Tìm kiếm địa chỉ IP của server.
   * Thiết lập kết nối TCP.
   * Gửi HTTP request.
   * Clean up.
* Session: là cách giao tiếp giữa client (chủ yếu là những thông tin tạm thời) với server. Giá trị của session sẽ được lưu trong một tệp tin trên máy chủ.
* Cookie: cũng được dùng để lưu những thông tin tạm thời, nhưng khác với session ở chỗ là nó được lưu ở phía client. Cẩn thận khi sử dụng cookie vì nó sẽ được gửi lên server nếu bạn cho phép.
* CORS: là một cơ chế cho phép nhiều tài nguyên khác nhau (fonts, Javascript, v.v…) của một trang web có thể được truy vấn từ domain khác với domain của trang đó. CORS là viết tắt của từ Cross-origin resource sharing. Và nó sinh ra vì chính sách same-origin policy.
* Cross-domain policy chỉ định các quyền mà ứng dụng khách web như Java, Adobe Flash, Adobe Reader, v.v. sử dụng để truy cập dữ liệu trên các tên miền khác nhau và được định nghĩa trong 1 file XML.

### 4.2. Web protocol
* Http longpolling: sinh ra để giải quyết http polling với ý tưởng: Client gửi 1 request và server sẽ giữ lại request đó và sẽ hồi đáp nó khi có một sự kiện tương ứng diễn ra ( nhưng sẽ có 1 trường hợp là request từ client đã đến timeout nhưng vẫn chưa có sự kiện mong đợi nào, khi đó, server sẽ buộc phải trả về 1 response nhưng có thể là không kèm theo bất kỳ dữ liệu có ích nào.)
* XMPP: Giống như HTTP, XMPP là một giao thức khách-máy chủ, nhưng nó khác với HTTP ở chỗ cho phép một trong hai bên gửi dữ liệu đến bên kia một cách không đồng bộ. Kết nối XMPP tồn tại lâu dài và dữ liệu được push thay vì pull.
* Websocket: là giao thức hỗ trợ giao tiếp hai chiều giữa client và server để tạo một kết nối trao đổi dữ liệu. Giao thức này không sử dụng HTTP mà thực hiện nó qua TCP.
* Server send events: tạo ra kết nối một chiều từ server đến client sử dụng giao thức HTTP truyền thống với định nghĩa dữ liệu đơn giản cho phía server (event stream format) và API gọn nhẹ phía client.

### 4.3. Javascript
* Angular là một JavaScript framework dùng để viết giao diện web (Front-end)
* ReactJS là một opensource được phát triển bởi Facebook, ra mắt vào năm 2013, bản thân nó là một thư viện Javascript được dùng để để xây dựng các tương tác với các thành phần trên website.
* Vuejs được gọi tắt là Vue. Là một framework rất linh động được dùng phổ biến để xây dựng nên các giao diện người dùng.

## 5. Thrift

* Apache thrift là một RPC thuộc loại stack software độc lập với ngôn ngữ lập trình với một cơ chế sinh code tự động có liên kết cho RPC.
* Thrift cho phép các xây dựng RPC Client và Server bằng cách chỉ việc định nghĩa kiểu dữ liệu và service interface trong một file định nghĩa đơn giản.
* Thrift IDL (Interface Definition Language) là một ngôn ngữ giao tiếp giúp sinh code của client và server như đã định nghĩa giúp cung cấp các server đa nền tảng.
* Thrift hỗ trợ hơn 20 ngôn ngữ lập trình.

### 5.1. Khái niệm Physical, Transport, Protocol, Processor trong Thrift
#### 5.1.1. Physical.
* Physical là tầng dưới cùng của Thrift, tầng Protocal sẽ ghi vào đây.
* Dễ hiểu hơn, physical là các thiết bị vật lý tham gia vào hệ thống.
#### 5.1.2. Transport
* Là tầng nằm trên Physical, chịu trách nhiệm về việc dữ liệu được truyền đi như thế nào, tầng này sẽ đọc và ghi vào Physical.
* Một số kiểu transport: 
    * TSocket: Sử dụng blocking socket I/O để vận chuyển.
    * TFramedTransport: Dữ liệu được truyền theo frame, ở kiểu này thì server phải thuộc loại non-blocking.
    * TFileTransport: Dữ liệu được ghi ra file (Chưa được impliment với java).
    * TMemoryTransport: Sử dụng memory để đọc ghi.
    * TZlibTransport: Dữ liệu được nén bởi zlib, cần phải kết hợp với các kiểu transport khác (Chưa được impliment với Java).
#### 5.1.3. Protocol
* Là tầng nằm trên Transport, chịu trách nhiệm về việc dữ liệu gì được truyền đi bằng cách cung cấp các serialize và deserialize.
* Thrift hỗ trợ cả 2 giao thức đó là text và binary.
* Một số kiểu của Protocol:
    * TBinaryProtocol: Mã hóa các giá trị số dưới dạng binary.
    * TCompactProtocol: Rất nhanh, mã hóa các dữ liệu cồng kềnh.
    * TDenseProtocol: Giống với TCompactProtocol nhưng dữ liệu truyền đi không có meta data, nhưng meta data sẽ được thêm lại khi dữ liệu được trả về (Chưa được impliment với Java).
    * TJSONProtocol: Sử dụng kiểu JSON khi mã hóa dữ liệu.
    * TSimpleJSONProtocol: Cũng sử dụng kiểu json nhưng thuộc loại write-only.
    * TDebugProtocol: Sử dụng kiểu text để hỗ trợ debug.
#### 5.1.4. Processor
* Processor nhận vào các tham số như một đầu vào và một đầu ra.
* Đọc dữ liệu từ đầu vào, sau đó xử lý dữ liệu và ghi dữ liệu vào đầu ra.
* Server sẽ truyền dữ liệu đầu vào đến Processor, các loại server cơ bản:
    * TSimpleServer: Không hỗ trợ đa luồng, sử dụng blocking I/O. Sử dụng để test là chủ yếu
    * TThreadPoolServer: Hỗ trợ đa luồng nhưng vẫn sử dụng blocking I/O.
    * TNonblockingServer: Hỗ trợ đa luồng, sử dụng non-blocking I/O.
### 5.2. Các kiểu dữ liêu của Thrift
#### 5.2.1. Các kiểu dữ liệu cơ bản
* bool: giá trị logic (true hoặc false)
* byte: giá trị nguyên 8bit có dấu.
* i16, i32, i64: tương ứng giá trị nguyên 16bit, 32bit và 64bit có dấu.
* double: giá trị số thực 64bit.
* string: giá trị văn bảng sử dụng mã hóa UTF-8.
#### 5.2.2. Các kiểu dữ liệu đặc biệt
* binary: giá trị một chuỗi các byte không được mã hóa.
#### 5.2.3. Structs
* Struct là một tập các trường có kiểu dữ liệu cơ bản, tương tự như struct trong C.
#### 5.2.4. Containers
* list: là interface tương ứng với C++ STL vector, Java ArrayList,...
* set: là interface tương ứng với STL set, Java HashSet,...
* map: là interface tương ứng với STL map, Java HashMap,...
#### 5.2.5. Exception
* Kế thừa các base class Exception tương ứng với từng ngôn ngữ lập trình.
#### 5.2.6. Service
* Bao gồm một tập các hàm, mỗi hàm có một tập các tham số và một kiểu trả về.

## 6. JavaEE, Java Core
* Java generics: Java 5 đưa vào khái niệm Generics. Với sự trợ giúp của Generics, bạn có thể tạo ra một đối tượng ArrayList chỉ cho phép chứa các phần tử có kiểu String, và không cho phép chứa các phần tử có kiểu khác.
* Thread: Thread cho phép một chương trình hoạt động hiệu quả hơn bằng cách thực hiện nhiều việc cùng một lúc. Các Thread có thể được sử dụng để thực hiện các tác vụ phức tạp trong nền mà không làm gián đoạn chương trình chính.
* 



