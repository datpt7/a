# HƯỚNG DẪN SETUP CƠ BẢN CHO NHÂN VIÊN MỚI
Mục lục
1. [Tạo tài khoản LDAP](#1)
2. [Tạo tài khoản Google OTP](#2)
3. [Kết nối vào VPN](#3)
4. [Gửi yêu cầu ACL](#4)
5. [Những request ACL cần làm](#5)
6. [SSH](#6)
7. [Sửa file hosts](#7)
# 1. Tạo tài khoản LDAP <a name="1"></a>
* Là tài khoản đăng nhập các tool nội bộ cơ bản như Doc, Git, được tạo & quản lý bởi team SO của ZTE. Bạn hãy gửi
mail tới `Zalo.SO` <zalo.so@vng.com.vn>, cc cho leader của bạn để yêu cầu "Tạo tài khoản LDAP". Trong mail ghi rõ
domain công ty của bạn.
* Ví dụ:
    * Subject: Tạo tài khoản LDAP
    * To: zalo.so@vng.com.vn
    * Cc: leader_email@vng.com.vn
    * Nội dung: 
        * Chào anh/chị, 
        * Em là `...` thuộc team Rnd. Hiện tại em đang cần tài khoản LDAP để sử dụng các tool docs, git của team. Nhờ SO tạo giúp em tài khoản LDAP `your_doamin`. 
        * Dạ em cảm ơn!

# 2. Tạo tài khoản Google OTP <a name="2"></a>
* Cài đặt ứng dụng `Google Authenticator` trên điện thoại
* Đăng nhập bằng tài khoản office vào link: https://security.vng.com.vn/token-gateway/
* Thiết lập OTP:
    * Nhấn vào nút `Activate` ở mục `Actions`
    * Giao diện sẽ hiển thị 3 bước hướng dẫn:
        * Authenticatior Mobile Apps: giới thiệu
        * Import key: Mở `Google Authenticator` vừa tải về để quét mã QR trên màn hình -> Nhấn next
        * Verify your indentity: Nhập mã bao gồm 6 số đang hiện trên điện thoại
* Thiếu lập PIN: 
    * Nhấn vào nút `Set PIN`
    * Nhập mã OTP đang hiện trên điện thoại

# 3. Kết nối vào VPN <a name="3"></a>
* Gửi yêu cầu vào VPN:
    * Truy cập vào link: https://security.vng.com.vn/jira2
    * Chọn đăng nhập băgnf VNGSSO (Microsoft Office 365)
    * Chọn `TS Service Desk` -> `System Operation` -> `VPN`
    * Nhập thông tin như sau và thay đổi phần `domain_của_bạn`: 
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-11-30_11-36-58.png)
> **_Lưu ý:_** Liên hệ với IT qua email helpdesk@vng.com.vn nếu cần trợ giúp
* Cấu hình máy tính kết nối VPN:
    * Đăng nhập vào link bằng G-OTP vừa thiết lập ở trên: https://ovpn.vng.com.vn
    * Chọn “Yourself (user-locked profile)” để tải về file thông tin đăng nhập cho máy tính của bạn (đuôi *.opvn)
    * Mở command line chạy lệnh: `sudo openvpn --config yourFileName.ovpn`. yourFileName.ovpn là file vừa tải về

# 4. Gửi yêu cầu ACL <a name="4"></a>
* Điều kiện: Phải kết nối được vào VPN
* Đăng nhập vào ACL: https://acl.vng.com.vn/
* Chọn request ACL ở góc trái trên
* Điền vào `VLAN/IP của người tạo yêu cầu mở ACL`:
    * VLAN ID / Account: Domain của bạn
    * IP address: Không cần điền, tự động sinh ra sau khi nhập domain
    * Port: Điền vào `any`
* Điền vào `VLAN/IP muốn kết nối đến hoặc ngược lại`:
    * IP Address: Ip của server cần truy cập
    * Port: Danh sách các port cách nhau bằng dấu phẩy

> **_Lưu ý:_**  Nếu gặp thông báo như sau thì chọn dòng chỉ có chữ `Zalo`
![](https://gitlab.com/datpt7/a/-/raw/main/Screenshot_from_2021-11-30_10-06-03.png)

* Điền vào mục lớn còn lại:
    * Protocol: Thông thường là TCP
    * Duration: Bỏ trống
    * Expiration: Chọn thời điểm hợp lý

* Cuối cùng là description, bạn điền lý do xin ACL
> **_Lưu ý:_** Lý do này cũng cần liên hệ manager/leader để viết cho đúng đắn.

# 5. Những request ACL cần làm <a name="5"></a>
||Ip|Port|
|:--:|:--|:--|
|zdoc|10.30.1.69|80, 443|
|git|10.30.65.254|80, 443|
|zminer|10.30.65.254|8022|
|staging|10.30.80.18|22, 80|
|dev|10.30.22.176|22, 80, 8084|

# 6. SSH <a name="6"></a>
* Điều kiện: Phải request ACL thì mới ssh được
* Vào terminal
* Tạo ssh key dùng rsa có gắn email: `ssh-keygen -t rsa -b 4096 -C your_email@vng.com.vn`
* Được hỏi nơi lưu: cứ để nguyên nơi mặc định
* Nhập 1 passphrase - có thể bỏ trống, chỉ enter cho chạy tiếp. Nếu có nhập thì phải lưu lại vì sau này mỗi lần
ssh vào server phải nhập lại passphrase này.
* file private key: `/home/<user>/.ssh/id_rsa`
* file public key: `/home/<user>/.ssh/id_rsa.pub`

# 7. Sửa file hosts <a name="7"></a>
Copy hết những host sau vào /etc/hosts
* 10.30.65.254 protect.zalo.services
* 10.30.1.69 docs.zalo.services

* 10.30.65.254 zmining.zaloapp.com
* 10.30.65.254 yarn-logs.zaloapp.com
* 10.30.65.254 yarn-resource.zaloapp.com
* 10.30.65.254 yarn3-history.zalo.services
* 10.30.65.254 yarn3-resource.zalo.services
* 10.30.65.254 yarn3-logs.zalo.services
* 10.30.65.254 metabase.zaloapp.com
* 10.30.65.254 zminer.zaloapp.com
* 10.30.65.254 server.zminer.zaloapp.com
* 10.30.65.254 zalogit2.zing.vn
* 10.30.65.254 dp.zaloapp.com
* 10.30.65.254 server.dp.zaloapp.com
* 10.30.65.254 clogin-logcentral.zaloapp.com
* 10.30.65.254 toolkit.zaloapp.com
* 10.30.65.254 zsvn.zaloapp.com
* 10.30.65.254 zdoc.zing.vn
* 10.30.65.254 nexus.infra.zalo.services
* 10.30.65.254 internal-challenge.zalo.ai
* 10.30.65.254 dashboard.zaloapp.com

* 10.30.65.254 ab.zaloapp.com
* 10.30.65.254 dev.ab.zaloapp.com
* 10.50.9.17 yarn.dev.rnd.zalo

* 10.50.9.17 spark.rnd.h2.zalo
* 10.30.65.254 jupyterhub-9-20-rnd.zalo.services
* 10.30.65.254 jupyterhub-9-17-rnd.zalo.services
* 10.30.65.254 ticket.zaloapp.com

* 10.30.1.69    protect.zalo.services
* 10.30.65.254 clogin-logcentral.zaloapp.com
