# *REPORT JAVA*


## *1. Lập trình hướng đối tượng (OOP: Object-Oriented Programming)*

### *1.1. Khái niệm*
* ***Lập trình hướng đối tượng*** là phương pháp lập trình lấy đối tượng làm nền tảng để xây dựng chương trình.
* Các khái niệm cơ bản đi kèm với lập trình hướng đối tượng bao gồm:
  * [*Đối tượng (Object)*](#12-đối-tượng)
  * [*Lớp (Class)*](#13-lớp)
* Lập trình hướng đối tượng có 4 tính chất:
  * [*Tính đóng gói (Encapsulation)*](#14-tính-đóng-gói)
  * [*Tính đa hình (Polymorphism)*](#15-tính-đa-hình)
  * [*Tính trừu tượng (Abstraction)*](#16-tính-trừu-tượng)
  * [*Tính kế thừa (Inheritance)*](#17-tính-kế-thừa)

### *1.2. Đối tượng*
* ***Đối tượng*** dùng để chỉ cho một thực thể (ví dụ: người, vật,...)
* Đối tượng bao gồm 2 thành phần:
  * Thuộc tính: đặc điểm của đối tượng.
  * Phương thức: thao tác, hành động của đối tượng.

### *1.3. Lớp*
* ***Lớp*** là một khuôn mẫu, đối tượng là một thể hiện cụ thể dựa trên khuôn mẫu đó (ví dụ: đối tượng *Chó Nhật* là thể hiện cụ thể của lớp *Chó*).
* Lớp bao gồm 2 thành phần chính:
  * Thuộc tính: đặc điểm của lớp.
  * Phương thức: thao tác, hành động của lớp.

### *1.4. Tính đóng gói*
* Các thuộc tính và phương thức có liên quan với nhau được đóng gói thành một lớp để tiện cho việc quản lý và sử dụng.
* Đồng thời tính đóng gói cũng giúp che giấu thông tin và chi tiết cài đặt nội bộ để bên ngoài không thể nhìn thấy.

### *1.5. Tính đa hình*
* Các đối tượng thuộc các lớp khác nhau có thể hiểu cùng một thông điệp theo các cách tùy chỉnh khác nhau.
* Ví dụ: Tiếng kêu của chó là "gâu gâu", còn tiếng kêu của mèo là "meo meo".

### *1.6. Tính trừu tượng*
* Khi thiết kế các đối tượng ta cần rút tỉa ra những đặc trưng chung của chúng rồi trừu tượng hóa thành các *interface* và thiết kế xem chúng sẽ tương tác với nhau như thế nào.

### *1.7. Tính kế thừa*
* Một lớp có thể sử dụng lại thuộc tính và phương thức của lớp khác giúp cho chương trình ngắn gọn hơn.
***


## *2. Lập trình hàm (FP: Functional Programing)*

### *2.1. Khái niệm*
* ***Lập trình hàm*** là phương pháp lập trình dựa trên các hàm toán học (function), tránh việc thay đổi giá trị của dữ liệu.
* Lập trình hàm có 2 tính chất:
  * [Purity](#22-purity)
  * [Immutability](#23-immutability)

### *2.2. Purity*
* Tất cả các function đều không có hiệu ứng phụ (side effect), không được tác động lên bất cứ giá trị nào bên ngoài nó, đồng thời cũng không sửa giá trị các đối số truyền vào.
* Chính vì thế, một pure function luôn cho ra cùng 1 kết quả với mỗi tập đối số truyền vào.

### *2.3. Immutability*
* Các đối tượng và các biến không bao giờ bị thay đổi giá trị.
***


## *3. Khái niệm Java virtual machine (JVM) và cách quản lý bộ nhớ trong JVM*

### *3.1. Khái niệm Java virtual machine (JVM)*
* ***Java virtual machine*** là máy ảo Java, được dùng để chạy các chương trình của Java, hay nói cách khác là trình thông dịch của Java.
* JVM hỗ trợ trên nhiều nền tảng khác nhau như windows, Linux, IOS,... vì thế cho nên Java có thể chạy được trên đa số các thiết bị hiện nay.
* JVM được sinh ra nhằm mục đích:
  * Dịch mã Java ra mã máy chạy được trên các hệ điều hành khác nhau.
  * Tăng tốc độ cho chương trình Java.
  * Nâng cao độ bảo mật và tránh virus phá source code.
* JVM có 3 thành phần chính:
  * Class Loader: tìm kiếm và load các file class vào vùng nhớ của Java dưới dạng bytecode.
  * Data Area: vùng nhớ được hệ thống cấp phát cho JVM.
  * Execution Engine: chuyển các lệnh của JVM trong file class thành các lệnh của máy, hệ điều hành tương ứng và thực thi chúng.

### *3.2. Cách quản lý bộ nhớ trong JVM*

#### *3.2.1. Heap memory*
* Dùng để lưu các đối tượng và dữ liệu động.
* Đây là vùng nhớ lớn nhất trong Data area.
* Quá trình dọn rác ở đây được JVM quản lý ([Garbage Collection](#326-cách-hoạt-động-của-garbage-collection) - viết tắt là GC).
* Điều chỉnh kích cỡ:
  * Ban đầu: `Xms`
  * Tối đa: `Xmx`
* Heap chia làm 2 phần:
  * Young generation space:
    * Dùng để lưu các đối tượng mới.
    * GC ở đây được gọi là "Minor GC".
    * Bao gồm 2 phần bộ nhớ:
      * Eden space: Dùng để lưu các đối tượng mới được khởi tạo.
      * 2 Survivor space: Dùng để lưu các đối tượng đã thông qua 1 hoặc nhiều quá trình "Minor GC" nhưng vẫn chưa đủ điều kiện để di chuyển đến Old generatin space. Tại mỗi thời điểm luôn có 1 survivor space còn trống.
      > **_Cụ thể về điều kiện:_** Mỗi đối tượng có một độ tuổi, mỗi lần chuyển qua lại giữa 2 survivor space thì tuổi của đối tượng sẽ tăng lên, khi tuổi đạt đến ngưỡng được quy định trước thì sẽ đối tượng đó sẽ đủ điều kiện để chuyển sang Old generation space.
  * Old generation space:
    * Dùng để lưu các đối tượng đã đủ điều kiện để chuyển từ Survivor space sang Old generation space (promote) hoặc các đối tượng bị chuyển trực tiếp từ Eden space vì Survivor không đủ bộ nhớ (premature promotion).
    * Quá trình GC ở đây được gọi là "Major GC".

#### *3.2.2. Thread Stacks*
* Bộ nhớ được lưu theo kiểu Stack.
* Mỗi luồng (thread) sẽ được cấp mỗi bộ nhớ Stack.
* Dùng để lưu dữ liệu tĩnh bao gồm các thông về các hàm, phương thức và cả con trỏ đến các đối tượng ở Heap.
* Quá trình dọn rác ở đây được hệ điều hành quản lý.
* Điều chỉnh kích cỡ: `Xss`

#### *3.2.3. Meta Space*
* Dùng để lưu thông tin về các lớp.
* Điều chỉnh kích cỡ: 
  * Kích cỡ: `XX:MetaspaceSize`
  * Lớn nhất: `XX:MaxMetaspaceSize`
  * Phần trăm nhỏ nhất sau khi dọn rác: `xx:MinMetaspaceFreeRatio`
  * Phần trăm lớn nhất sau khi dọn rác: `xx:MaxMetaspaceFreeRatio`
> **_Cụ thể về điều kiện:_** Đây là thay thế của PermGen memory space bắt đầu từ version 8. Sự khác biệt cơ bản của Meta và PermGen là bộ nhớ của Meta có thể tự động tăng trong khi PermGen thì không thể.

#### *3.2.4. Code Cache*
* Đây là nơi JIT (Just In Time) lưu trữ những code (đã được biên dịch) thường xuyên được xử dụng.

#### *3.2.5. Shared Libraries*
* Bao gồm những file thư viện được sử dụng bởi nhiều ứng dụng, giúp giảm số lựong thư viện trùng lặp trong hệ thống.

#### *3.2.6. Cách hoạt động của Garbage Collection*
* Garbage collection chịu trách nhiệm về:
  * Cấp phát bộ nhớ từ hệ điều hành và trả về hệ điều hành.
  * Cung cấp giá trị của các vùng nhớ khi được yêu cầu từ ứng dụng.
  * Xác định vùng nhớ nào còn đang được xử dụng bởi ứng dụng.
  * Làm sạch những vùng nhớ nào không còn được sử dụng.
* Thuật toán thường được sử dụng trong Garbage collection là Mark & Sweep bao gồm 3 quá trình: 
  * Marking: Đánh dấu vùng nhớ nào đang được sử dụng và không được sử dụng.
  * Sweeping: Làm sách các vùng nhớ không còn được sử dụng.
  * Compacting: Chuyển các vùng nhớ đang được sử dụng lại với nhau để  vùng trống được tối đa.
* Như đã đề cập ở phần trên: GC có 2 loại: Minor GC và Major GC
  * Minor GC
    * Hoạt động ở Young generation space.
    * Được kích hoạt khi Eden space bị đầy không thể cấp phát vùng nhớ để lưu đối tượng mới.
    * Khi Minor GC được kích hoạt nhiều lần, đương nhiên các vùng nhớ không còn được sử dụng sẽ bị làm sạch, các vùng nhớ còn được sử dụng sẽ được chuyển qua lại giữa 2 Survivor space nếu chưa đủ tuổi, còn khi đủ tuổi sẽ được chuyển thẳng vào Young generation space.
  * Major GC
    * Hoạt động ở Old generation space.
    * Được kích hoạt khi:
      * Một trong các lệnh sau đây được gọi: `System.gc()`, `Runtime.getRuntime().gc()`.
      * Old generation space bị đầy không thể cấp phát vùng nhớ để chuyển từ Young generation space.
      * Bộ nhớ tối đa được cấp cho Meta space quá nhỏ không đủ để lưu các lớp mới.
***


## *4. Khái niệm và cách sử dụng build tool*

### *4.1. Khái niệm*
* ***Build tool*** là công cụ giúp tự động hóa mọi công việc để tạo ra các ứng dụng từ source code, bao gồm build, complier, kiểm thử và đóng gói source code thành một hình thức có thể được sử dụng hoặc thực thi được.
* Một build tool được sử dụng phổ biến hiện nay là "Gradle"

### *4.2 Cách sử dụng một build tool cụ thể: Gradle*

#### *4.2.1. Khái quát sơ lược lại về Gradle*
* ***Gradle*** là một build automation tool và open-source.
* Gradle rất linh hoạt đủ để build hầu hết các loại phần mềm.
* Gradle kết hợp các ưu điểm của 2 build tool khác là Ant và Maven, đồng thời cũng thêm vào các cải thiện mới giúp Gradle ngày càng trở nên mạnh mẽ.
* Các ưu điểm của Gradle:
  * Hỗ trợ cho 60 ngôn ngữ lập trình khác nhau như: Java, Scala, Python,...
  * Có thể tích hợp nhiều công cụ IDE, CI, deploy.
  * Quản lý dependency mạnh mẽ nhờ vào: định nghĩa đơn giản, có thể nhận bất kì repository nào được biết đến, giải pháp dependency tối ưu, giải quyết xung đột.
  * Hỗ trợ build reporting.

### *4.2.2. Cách sử dụng Gradle bằng CLI*
* Nếu đã có một poject được tạo bởi Gradle thì không cần phải tải Gradle, mà chỉ cần chạy các file gradlew hoặc gradlew.bat tùy vào hệ điều hành.
* Nếu muốn thêm Gradle vào 1 project đã tồ n tại thì đầu tiên phải tải gradle, sau đó vào thư mục gốc của project và chạy câu lệnh: `gradle wrapper`, nếu bạn muốn chỉ định một version khác thì thêm `--gradle-version [gradle_version]`.
* Nếu muốn tạo mới một project với Gradle thì đầu tiên vẫn phải tải Gradle, sau đó tạ vào vào thư mục gốc cho project và chạy lênh: `gradle init`.
* Cài đặt những thứ cần sử dụng để build (dependency,...) vào file `ten_model/build.gradle(.kts)`.
* Tên thư mục gốc và các model ở file: `settings.gradle(.kts)`.

* Các task cơ bản của Gradle:
  * Chạy project: `gradle run`.
  * Chạy test cho project: `gradle test`, kết quả của test sẽ lưu dưới dạng file html ở thư mục: `thu_muc_goc/ten_module/build/reports/tests/`.
  * Build project: `gradle build`, 2 file sẽ được tạo với định dạng tar và zip được lưu ở thư mục: `ten_module/build/distributions/`.
***


## *5. Khái niệm các loại tests trong software development*

### *5.1. Kiểm thử chức năng - Functional testing*
* Kiểm thử chức năng là kiểm tra xem hệ thống có hoạt động đúng theo các yêu cầu nghiệp vụ hay không.
* Kiểm thử chức năng được thực hiện ở tất cả các mức kiể m thử.
* Kiểm thử chức năng hoạt động theo 2 quan điểm: requirements-based và business-process-bassed.

### *5.2. Kiểm thử phi chức năng - Non functional testing*
* Kiểm thử phi chức năng là đặc tính chất lượng của hệ thống sẽ được kiểm tra.
* Những thứ cần quan tâm ở loại kiểm thử này: chức năng, độ tin cậy, khả năng sử dụng, tính hiệu quả, khả năng bảo trì.

### *5.3. Kiểm thử cấu trúc - Structural testing*
* Kiểm thử cấu trúc là kiểm thử dựa trên phân tích cấu trúc bên trong của thành phần hoặc hệ thống.
* Kiểm thử cấu trúc chủ yếu được áp dụng ở iểm thử thành phần và kiểm thử tích hợp.

### *5.4. Kiểm thử xác nhận và hồi quy*
* Kiểm thử xác nhận là kiểm thử xem lỗi sau khi sửa đã hoạt động được chưa.
* Kiểm thử hồi quy là kiểm thử xem lỗi sau khi được sửa đã hoạt động mà không làm ảnh hưởng đến các chức năng khác hay không.





