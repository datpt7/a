# ProductionUploads
## 1. Tổng quan
### 1.1. Mục đích của project
- Upload database trên production
### 1.2. Kiến trúc của project
- Project bao gồm 3 module: crond-schedules, luigi-orchestration, database-uploads 
#### 1.2.1. database-uploads <a name="database_uploads"></a>
- Được viết bằng Java.
- Sử dụng [Apache Spark](#https://spark.apache.org/) để put database, dùng [Gralde](https://gradle.org/) để tạo gói jar, các gói jar này sẽ được submit lên hệ thống [Apache Hadoop Yarn](#https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YARN.html) trong hệ thống ZTE. Theo dõi các Spark job tại [server 10.30.22.56](http://10.30.22.56:8088/cluster/app) và [server 10.30.58.162](http://10.30.58.162:18080/).
#### 1.2.2. luigi-orchestration
- Được viết bằng Python.
- Sử dụng framework [Luigi](https://en.wikipedia.org/wiki/Luigi) để quản lý các task data pipeline. Luigi có nhiệm vụ submit các spark job trong module [database-uploads](#database_uploads). Xem tất cả các task của luigi trong Luigi Task Visualizer.
#### 1.2.1. crond-schedules
- Bao gồm nhiều file .cron (các file cấu hình cho Crontab). Crontab có nhiệm vụ kích hoạt các tác vụ trong module luigi-orchestration theo định kỳ (ví dụ: mỗi phút, mỗi giờ, mỗi ngày ...). Log của crontab được ghi trong /var/log/cron.

## 2. Các task đã làm trong project
- Tạo 2 class upload cho demographic với noisedId:
   - RealName
   - UserAge
