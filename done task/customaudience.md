# Customaudience
## 1. Tổng quan
### 1.1. Mục đích của project
- Project theo dõi số lượng người dùng zalo có trạng thái cụ thể nào đó (ví dụ: số người dùng đã bấm quảng cáo zalo).
- Input: Đọc dữ liệu từ zlogcentral_live trong Hadoop HDFS. 
- Output: kết quả sau khi được xử lý sẽ được đưa vào cơ sở dữ liệu cho mỗi trạng thái với 5 khoảng thời gian:
  - d01 (hôm qua)
  - d07 (7 ngày qua)
  - d30 (30 ngày qua)
  - d90 (90 ngày qua)
  - inf (vô cùng)
### 1.2. Kiến trúc của project
Project bao gồm 5 module: crond-schedules, luigi-orchestration, spark-transforms, database-uploads và spark-uploads-yarn
#### 1.2.1. crond-schedules 
- Bao gồm nhiều file .cron (các file cấu hình cho [Crontab](https://en.wikipedia.org/wiki/Cron)). Crontab có nhiệm vụ kích hoạt các tác vụ trong module [luigi-orchestration](#luigi_orchestratio) theo định kỳ (ví dụ: mỗi phút, mỗi giờ, mỗi ngày ...). Log của crontab được ghi trong `/var/log/cron`.
#### 1.2.2. luigi-orchestratio <a name="luigi_orchestratio"></a>
- Được viết bằng Python.
- Sử dụng framework [Luigi](https://en.wikipedia.org/wiki/Luigi) để quản lý các task data pipeline. Luigi có nhiệm vụ thực thi các gói jar trong module [database-uploads](#database_uploads), submit các spark job trong module [spark-transforms](#spark_transforms) và [database-uploads-yarn](#database_uploads_yarn). Xem tất cả các task của luigi trong Luigi Task Visualizer.
#### 1.2.3. spark-transforms <a name="spark_transforms"></a>
- Được viết bằng Scala.
- Sử dụng [Apache Spark](#https://spark.apache.org/) để xử lý dữ liệu từ input, dùng [sbt assembly](https://github.com/sbt/sbt-assembly/blob/develop/README.md) để tạo gói jar, các gói jar này sẽ được submit lên hệ thống [Apache Hadoop Yarn](#https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YARN.html) trong hệ thống ZTE. Theo dõi các Spark job tại [server 10.30.22.56](http://10.30.22.56:8088/cluster/app) và [server 10.30.58.162](http://10.30.58.162:18080/).
#### 1.2.4. database-uploads <a name="database_uploads"></a>
- Được viết bằng Java.
- Có nhiệm vụ ghi kết quả cuối cùng vào cơ sở dữ liệu ZTE. Sử dụng Gradle để tạo gói jar. 
>> **Lưu ý:** Hãy nhớ rync thư mục conf với tệp jar. 
#### 1.2.5. database-uploads-yarn <a name="database_uploads_yarn"></a>
- Được viết bằng Java.
- Có nhiệm vụ giống như [database-uploads](#database_uploads) nhưng sử dụng Apache Spark giống như [spark-transforms](#spark_transforms)

## 2. Các task đã làm trong project
### 2.1. crond-schedules
- Setup crontab cho task Union d30 cho zalo_user_noisedId trong zaloaudience
- Setup crontab cho task Union d30 cho zalo_feed_noisedId đã được noise trong zaloaudience
### 2.2. luigi-orchestratio
- Thêm 1 task extract theo ngày trong customaudience.extrac_tasks: DailyExtractTask
- Chuyển task trên để extract theo giờ thay vì theo ngày:
   - zaloaudience.zalo_user
   - zaloaudience.zalo_user_noisedId
   - zaloaudience.zalo_feed
   - zaloaudience.zalo_feed_noisedId
   - apptracking.apptracking
   - globalidapp.baomoi
### 2.3. spark-transforms
- Thêm 1 class lấy user_Noisedid
### 2.4. database-uploads
- Chuyển file build.gradle từ groovy sang kotlin
### 2.5. database-uploads-yarn
- Chuyển file build.gradle từ groovy sang kotlin




