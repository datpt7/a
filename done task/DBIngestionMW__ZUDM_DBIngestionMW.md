# ZUDM_DBIngestionMW
## 1. Tổng quan
### 1.1. Mục đích của project
- Sử dụng để  put db đối với những tác vụ có bắn eventbus (cũng như những như tác vụ không bắn eventbus nhưng có nhu cầu custom cao).
### 1.2. Kiến trúc của project
- Project bao gồm 3 thành phần: dbingestionmw-thrift, ZUDM_DBIngestionMWService, dbingestionmw-client
#### 1.2.1. dbingestionmw-thrift
- Được viết bằng Thrift.
- Định nghĩa các API chính của middleware. Sử dụng Gradle để tạo gói jar với tên jzudmdbingestionmw-thrift9-{VERSION}.jar.
#### 1.2.2. ZUDM_DBIngestionMWService
- Được viết bằng Java.
- Import gói jar và handle cách xử lý request cho từng API được định nghĩa trước đó ở phần 1. 
#### 1.2.3. dbingestionmw-client
- Được viết bằng Java.
- Import gói jar và handle các API ở phía client được định nghĩa trước đó ở phần 1. Sử dụng Gradle để tạo gói jar với tên zudmdbingestionmwclient-{VERSION}.jar
## 2. Các task đã làm trong project
### 2.1. dbingestionmw-thrift
- Định nghĩa thêm 4 API cho demographic:
   - ingestAgeNoisedId
   - ingestGenderNoisedId
   - ingestRealNameNoisedId
   - ingestMaritalStatusNoisedId
### 2.2. ZUDM_DBIngestionMWService
- Handle cho 4 API vừa định nghĩa thêm ở trên
### 2.3. dbingestionmw-client
- Handle cho 2 API ở phía client:
   - uploadRealnameNoisedId
   - uploadUserAgeNoisedId


