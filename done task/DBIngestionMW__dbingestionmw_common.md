# dbingestionmw_common
## 1. Tổng quan 
### 1.1. Mục đích của project
- Mỗi API upload sẽ có 1 KeyType tương ứng. Việc quản lý KeyType giúp xác định thông tin chính xác số lượng KeyType trong từng project (cũng như từng database).
### 1.2. Kiến trúc của project
- Viết bằng Java.
- Dùng gradle để tạo gói jar với tên zudmdbingestionmwcommon-{VERSION}.jar
## 2. Các task đã làm trong project
- Tạo 1 class giúp gen ra key từ id và 1 class gen ra id từ key 
- Tạo các KeyType với noisedId cho các project:
   - DemographicPredictionKey
   - FintechKey
   - FraudKey
   - OccupationKey





